package mph.entity;

import static mph.util.MagicNumbers.DELIVERY_GRACE_PERIOD_HOURS;
import static mph.util.MagicNumbers.MAX_STUDENTS_PER_GROUP;
import static mph.util.MagicNumbers.MIN_SCORE;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mph.exceptions.GroupEmptyException;
import mph.exceptions.GroupLimitReachedException;
import mph.exceptions.StudentNotInGroupException;
import mph.util.GroupDetails;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * A group of students, working on a project
 * 
 * @author Gabriele Petronella
 */
@Entity
@Table(name="Team") //'Group' is a keyword in MySQL! <- It took me days to figure this out... I'm an idiot...
public class Group {

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	/**
	 * The final evaluation for the group
	 */
	private int finalScore;

	/**
	 * The group name
	 */
	@Column(unique=true, nullable=false)
	private String name;

	/**
	 * The students composing the group
	 */
	@ManyToMany
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinTable(name="Student_Team",
				joinColumns=@JoinColumn(name="student_id"),
				inverseJoinColumns=@JoinColumn(name="group_id"),
				uniqueConstraints=@UniqueConstraint(columnNames={"student_id", "group_id"}))
	private List<Student> members;
	
	/**
	 * The documents delivered by the group
	 */
	@OneToMany(mappedBy="group")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Document> documents;
	
	/**
	 * The project the group is working on
	 */
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="project_id", updatable=false, nullable=false)
	private Project project;
	
	public Group() {}
	

	/**
	 * @param name
	 * @param project
	 */
	public Group(String name, Project project) {
		this.name = name;
		this.project = project;
		this.members = new ArrayList<Student>();
		this.documents = new ArrayList<Document>();
		this.finalScore = 0;
	}
	
	//Getters and setters
	/**
	 * @return
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return
	 */
	public int getFinalScore() {
		return finalScore;
	}
	
	/**
	 * @param finalScore
	 */
	public void setFinalScore(int finalScore) {
		this.finalScore = finalScore;
	}
	
	/**
	 * @return
	 */
	public List<Student> getMembers() {
		return members;
	}
	
	/**
	 * @param members
	 */
	public void setMembers(List<Student> members) {
		this.members = members;
	}
	
	/**
	 * @return
	 */
	public List<Document> getDocuments() {
		return documents;
	}
	
	/**
	 * @param documents
	 */
	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}
	
	/**
	 * @return
	 */
	public Project getProject() {
		return project;
	}
	
	/**
	 * @param project
	 */
	public void setProject(Project project) {
		this.project = project;
	}
	
	/**
	 * @param student
	 * @throws GroupLimitReachedException
	 */
	public void addStudent(Student student) throws GroupLimitReachedException {
		if (this.getMembers().size() >= MAX_STUDENTS_PER_GROUP) {
			throw new GroupLimitReachedException();
		}
		this.getMembers().add(student);
	}
	
	/**
	 * @param student
	 * @throws GroupEmptyException
	 */
	public void delStudent(Student student) throws GroupEmptyException, StudentNotInGroupException {
		if (this.getMembers().size() <= 0) {
			throw new GroupEmptyException();
		}
		if (this.getMembers().contains(student)) {
			this.getMembers().remove(student);
		} else {
			throw new StudentNotInGroupException();
		}
	}
	
	/**
	 * Assigns the final score.
	 * 
	 * The final score is the rounded average of the delivered docs scores
	 * - If not every delivered document has been evaluated the final score is 0
	 * - If a document is not delivered, it's counted as MIN_SCORE after a GRACE_TIME_PERIOD
	 *   allowed for late delivery.
	 */
	public void assignFinalEvaluation() {
		int dueDocuments = this.getProject().getDeliverables().size();
		int deliveredDocuments = this.getDocuments().size();
		
		//If not every deliverable has a document associated
		if (deliveredDocuments < dueDocuments) {
			Calendar threshold = GregorianCalendar.getInstance();
			threshold.add(Calendar.HOUR, -DELIVERY_GRACE_PERIOD_HOURS);
			for (Deliverable d : this.getProject().getDeliverables()) {
				//If not all the deliverable deadlines are expired, return
				if (d.getDeadline().after(threshold)) {
					this.setFinalScore(0);
					return;
				}
			}
		}
		
		//At this point every deliverable has 1 document or the delivery period has expired
		
		float sum = 0;
		for (Document d : this.getDocuments()) {
			//Sum all the evaluated documents
			if (d.getScore() > 0) {
				sum += d.getFinalScore();
			} else {
				//If exists a document not evaluated, return
				this.setFinalScore(0);
				return;
			}
		}
		
		//Add MIN_SCORE for every document not delivered
		sum += MIN_SCORE * (dueDocuments - deliveredDocuments);
		
		int avg = Math.round(sum/(float)dueDocuments);
		
		this.setFinalScore(avg);
	}

	public GroupDetails asGroupDetails() {
		List<Integer> membersIds = new ArrayList<Integer>();
		List<Integer> documentsIds = new ArrayList<Integer>();

		for (Student s : this.getMembers()) {
			membersIds.add(new Integer(s.getId()));
		}
		for (Document d : this.getDocuments()) {
			documentsIds.add(new Integer(d.getId()));
		}
		return new GroupDetails(this.getId(), this.getName(), this.getProject().getId(), this.getFinalScore(), membersIds, documentsIds);
	}
	
	/**
	 * Two groups are the same if they share the same name
	 */
	@Override
	public boolean equals(Object that) {
		if (this == that)
			 return true;
		if (that == null) {
			return false;
		}
		if (!(that instanceof User)) {
			return false;
		}
		Group thatGroup = (Group)that;
		return thatGroup.getName().equals(this.getName());
	}
	
	@Override
	public int hashCode() {
	    return this.getName().hashCode();
	}
}
