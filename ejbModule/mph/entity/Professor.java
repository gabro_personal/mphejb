package mph.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

import mph.util.ProfessorDetails;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * A professor
 * 
 * @author Gabriele Petronella
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Professor extends User {
	private static final long serialVersionUID = 1L;
	
	/**
	 * The projects created by the professor
	 */
	@OneToMany(mappedBy="professor", cascade={CascadeType.ALL})
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Project> projects;
	
	public Professor() {}
	
	/**
	 * @param username
	 * @param password
	 * @param firstName
	 * @param lastName
	 * @param avatarData
	 */
	public Professor(String username, String password, String firstName,
			String lastName, byte[] avatarData) {
		super(username, password, firstName, lastName, avatarData);
		this.projects = new ArrayList<Project>();
	}
	
	//Getters and setters
	/**
	 * @return
	 */
	public List<Project> getProjects() {
		return projects;
	}
	
	/**
	 * @param projects
	 */
	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}
	
	public ProfessorDetails asProfessorDetails() {
		List<Integer> projectsIds = new ArrayList<Integer>();
		for (Project p : this.getProjects()) {
			projectsIds.add(new Integer(p.getId()));
		}
		return new ProfessorDetails(this.getId(), 
									this.getUsername(),
									this.getFirstName(),
									this.getLastName(), 
									projectsIds,
									this.getAvatarData());
	}
}