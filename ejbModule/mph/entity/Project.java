package mph.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import mph.util.ProjectDetails;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * A project created by a professor
 * 
 * @author Gabriele Petronella
 */
@Entity
public class Project implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue
	private int id;
	
	/**
	 * The project name
	 */
	@Column(unique=true, nullable=false)
	private String name;
	
	/**
	 * The project description
	 */
	@Lob //The description may be long so it's better to save it as a binary object (and to avoid indexing on it)
	private String description;
	
	/**
	 * The professor who created the project
	 */
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="professor_id", updatable=false, nullable=false)
	private Professor professor;

	/**
	 * The groups working on the project
	 */
	@OneToMany(mappedBy="project")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Group> groups;
	
	/**
	 * The deliverable defined for the project
	 */
	@OneToMany(mappedBy="project")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Deliverable> deliverables;

	public Project() {}
	
	/**
	 * @param name
	 * @param description
	 * @param professor
	 */
	public Project(String name, String description, Professor professor) {
		this.name = name;
		this.description = description;
		this.professor = professor;
		this.deliverables = new ArrayList<Deliverable>();
		this.groups = new ArrayList<Group>();
	}
	
	//Getters and setters
	/**
	 * @return
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return
	 */
	public Professor getProfessor() {
		return professor;
	}
	
	/**
	 * @param professor
	 */
	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	/**
	 * @return
	 */
	public List<Group> getGroups() {
		return groups;
	}

	/**
	 * @param groups
	 */
	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}
	
	/**
	 * @return the deliverable
	 */
	public List<Deliverable> getDeliverables() {
		return deliverables;
	}

	/**
	 * @param deliverable the deliverable to set
	 */
	public void setDeliverables(List<Deliverable> deliverables) {
		this.deliverables = deliverables;
	}
	
	public void addDeliverable(Deliverable d) {
		this.getDeliverables().add(d);
	}

	/**
	 * @return
	 */
	public ProjectDetails asProjectDetails() {
		List<Integer> groupsIds = new ArrayList<Integer>();
		List<Integer> deliverablesIds = new ArrayList<Integer>();
		for (Group g : this.getGroups()) {
			groupsIds.add(new Integer(g.getId()));
		}
		for (Deliverable d : this.getDeliverables()) {
			deliverablesIds.add(new Integer(d.getId()));
		}
		return new ProjectDetails(	this.getId(), 
									this.getName(), 
									this.getDescription(), 
									this.getProfessor().getId(), 
									groupsIds,
									deliverablesIds);
	}
	
	/**
	 * Gets all the students from the groups working on the project
	 * 
	 * @return a list of Student
	 */
	public List<Student> getStudents() {
		List<Student> students = new ArrayList<Student>();
		for (Group g : this.getGroups()) {
			students.addAll(g.getMembers());
		}
		return students;
	}
	
	/**
	 * Two projects are the same if they share the same name
	 */
	@Override
	public boolean equals(Object that) {
		if (this == that)
			 return true;
		if (that == null) {
			return false;
		}
		if (!(that instanceof User)) {
			return false;
		}
		Project thatProj = (Project)that;
		return thatProj.getName().equals(this.getName());
	}
	
	@Override
	public int hashCode() {
	    return this.getName().hashCode();
	}
}
