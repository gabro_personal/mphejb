package mph.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import mph.util.DeliverableDetails;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * A deliverable associated with a project
 *
 * @author Gabriele Petronella
 */
@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"name", "project_id"})})
public class Deliverable implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue
	private int id;
	
	/**
	 * The name of the deliverable. It's unique inside a project.
	 */
	@Column(nullable=false)
	private String name;
	
	/**
	 * A brief description of the deliverable
	 */
	private String description;
	
	/**
	 * A due date for the deliverables
	 */
	@Column(nullable=false)
	private Calendar deadline;
	
	/**
	 * The project which the deliverable is associated with
	 */
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="project_id", nullable=false, updatable=false)
	private Project project;
	
	/**
	 * Delivered documents that refers to the deliverable
	 */
	@OneToMany(mappedBy="deliverable", cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Document> documents;
	
	public Deliverable() {}
	

	/**
	 * @param project
	 * @param name
	 * @param description
	 * @param deadline
	 */
	public Deliverable(Project project, String name, String description, Calendar deadline) {
		this.project = project;
		this.name = name;
		this.description = description;
		this.deadline = deadline;
		this.documents = new ArrayList<Document>();
	}
	
	/**
	 * @return
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the deadline
	 */
	public Calendar getDeadline() {
		return deadline;
	}

	/**
	 * @param deadline the deadline to set
	 */
	public void setDeadline(Calendar deadline) {
		this.deadline = deadline;
	}

	/**
	 * @return
	 */
	public Project getProject() {
		return project;
	}
	
	/**
	 * @param project
	 */
	public void setProject(Project project) {
		this.project = project;
	}
	
	/**
	 * @return
	 */
	public List<Document> getDocuments() {
		return documents;
	}
	
	public void addDocument(Document d) {
		this.getDocuments().add(d);
	}
	
	/**
	 * @param documents
	 */
	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}
	
	public DeliverableDetails asDeliverableDetails() {
		List<Integer> docsIds = new ArrayList<Integer>();
		for (Document d : this.getDocuments()) {
			docsIds.add(new Integer(d.getId()));
		}
		return new DeliverableDetails(	this.getId(),
										this.getProject().getId(), 
										this.getName(), 
										this.getDescription(),
										this.getDeadline(),
										docsIds);
	}
	
	/* 
	 * Two deliverables are the same if they share the name and the project
	 */
	@Override
	public boolean equals(Object that) {
		if (this == that)
			 return true;
		if (that == null) {
			return false;
		}
		if (!(that instanceof User)) {
			return false;
		}
		Deliverable thatDeliv = (Deliverable)that;
		return thatDeliv.getName().equals(this.getName())
			&& thatDeliv.getProject().getName().equals(this.getProject().getName());
	}
	
	@Override
	public int hashCode() {
	    int hash = 1;
	    hash = hash * 31 + this.getName().hashCode();
	    hash = hash * 31 + this.getProject().hashCode();
	    return hash;
	}
}
