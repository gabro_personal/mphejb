package mph.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

/**
 * A persistent login session associated to an authenticated user
 * 
 * @author Gabriele Petronella
 */

@Entity
public class LoginSession implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * The session begin time
	 */
	private Calendar timestamp;
	
	/**
	 * The user who started the session
	 */
	@OneToOne(mappedBy="currentSession")
	private User user;
	
	@Id @GeneratedValue(generator="uuid-generator")
	@GenericGenerator(name="uuid-generator", strategy = "uuid")
	private String id;
	
	/**
	 * Creates a new LoginSession
	 */
	public LoginSession() {
		this.timestamp = GregorianCalendar.getInstance();
	}

	/**
	 * Two sessions are the same if they share the same timestamp
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoginSession that = (LoginSession) obj;
		if (timestamp == null) {
			if (that.timestamp != null)
				return false;
		} else if (!timestamp.equals(that.timestamp))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		return timestamp.hashCode();
	}

	@Override
	public String toString() {
		return timestamp.toString();
	}

	/**
	 * @return
	 */
	public Calendar getTimestamp() {
		return timestamp;
	}
	
	/**
	 * @return
	 */
	public User getUser() {
		return this.user;
	}

	/**
	 * Unique identifier
	 * 
	 * @return the unique id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * renews the session, updating the timestamp to the current time
	 */
	public void renew() {
		this.timestamp = GregorianCalendar.getInstance();
	}
}
