package mph.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;

import mph.exceptions.StudentNotInGroupException;
import mph.util.StudentDetails;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * A student
 * 
 * @author Gabriele Petronella
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Student extends User {
	private static final long serialVersionUID = 1L;

	/**
	 * The student matriculation number
	 */
	@Column(unique=true)
	private String matrNo;

	/**
	 * The groups the student is working in
	 */
	@ManyToMany(mappedBy="members")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Group> groups;
	
	public Student() {}

	public Student(String username, String password, String firstName,
			String lastName, String matrNo, byte[] avatarData) {
		super(username, password, firstName, lastName, avatarData);
		this.matrNo = matrNo;
		this.groups = new ArrayList<Group>();
	}

	//Getters and setters
	/**
	 * @return
	 */
	public String getMatrNo() {
		return matrNo;
	}
	
	/**
	 * @param matrNo
	 */
	public void setMatrNo(String matrNo) {
		this.matrNo = matrNo;
	}
	
	/**
	 * @return
	 */
	public List<Group> getGroups() {
		return groups;
	}
	
	/**
	 * @param group
	 */
	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}
	
	public void addGroup(Group g) {
		this.groups.add(g);
	}
	
	public void delGroup(Group g) throws StudentNotInGroupException {
		if(this.groups.contains(g)) {
			this.groups.remove(g);
		} else {
			throw new StudentNotInGroupException();
		}
	}
	
	@Override
	public String toString() {
		return "Student: <" +
							"id:"+this.getId()+", "+
							"name:"+this.getFirstName()+", "+
							"lastName:"+this.getLastName()+", "+
							"matrNo:"+this.getMatrNo()+", "+
//							"group id:"+this.getGroups().getId()+", "+
						">";
	}
	
	public StudentDetails asStudentDetails() {
		List<Integer> groupsIds = new ArrayList<Integer>();
		for (Group g : this.getGroups()) {
			groupsIds.add(new Integer(g.getId()));
		}
		return new StudentDetails(	this.getId(), 
									this.getUsername(),
									this.getFirstName(),
									this.getLastName(), 
									this.getMatrNo(),
									groupsIds,
									this.getAvatarData());
	}
}
