package mph.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

import org.jboss.security.auth.spi.Util;

/**
 * A generic user of the system
 *  
 * @author Gabriele Petronella
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "UserType")
public abstract class User implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue
	private int id;
	
	/**
	 * The user username 
	 */
	@Column(unique=true)
	private String username;
	
	/**
	 * The user password
	 */
	@Column(nullable=false)
	private String password; //Stored as SHA1 hash
	
	/**
	 * The user first name
	 */
	private String firstName;
	
	/**
	 * The user last name 
	 */
	private String lastName;
	
	/**
	 * The user current login session (possibly none) 
	 */
	@OneToOne
	private LoginSession currentSession;
	
	/**
	 * The user avatar
	 */
	@Lob
	@Column(columnDefinition="MEDIUMBLOB") //up to 16Mb
	private byte[] avatarData;
	
	public User() {}

	/**
	 * @param username
	 * @param password
	 * @param firstName
	 * @param lastName
	 * @param avatarData
	 */
	public User(String username, String password, String firstName,
			String lastName, byte[] avatarData) {
		this.username = username;
		this.setPassword(password);
		this.firstName = firstName;
		this.lastName = lastName;
		this.avatarData = avatarData;
	}
	
	//Getters and setters
	/**
	 * @return
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	/**
	 * @return
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * @param password
	 */
	public void setPassword(String password) {
		 this.password = Util.createPasswordHash("SHA-1", Util.BASE64_ENCODING, null, null, password);
	}
	
	/**
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return
	 */
	public LoginSession getCurrentSession() {
		return currentSession;
	}

	/**
	 * @param currentSession
	 */
	public void setCurrentSession(LoginSession currentSession) {
		this.currentSession = currentSession;
	}
	
	/**
	 * @return
	 */
	public byte[] getAvatarData() {
		return avatarData;
	}

	/**
	 * @param avatarData
	 */
	public void setAvatarData(byte[] avatarData) {
		this.avatarData = avatarData;
	}

	public Boolean checkPassword(String passwordToCheck) {
		String hashToCheck = Util.createPasswordHash("SHA-1", Util.BASE64_ENCODING, null, null, passwordToCheck);
		return this.getPassword().equals(hashToCheck);
	}
	
	/**
	 * Two users are the same if they share the same username
	 */
	@Override
	public boolean equals(Object that) {
		if (this == that)
			 return true;
		if (that == null) {
			return false;
		}
		if (!(that instanceof User)) {
			return false;
		}
		User thatUser = (User)that;
		return thatUser.getUsername().equals(this.getUsername());
	}
	
	@Override
	public int hashCode() {
		return this.username.hashCode();
	}
}
