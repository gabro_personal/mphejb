package mph.entity;

import static mph.util.MagicNumbers.*;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import mph.util.DocumentDetails;

/**
 * A document delivered by a group, referring to a deliverable
 * 
 * @author Gabriele Petronella
 */
@Entity
public class Document implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue
	private int id;
	
	/**
	 * The pdf file representing the document
	 */
	@Lob
	//MEDIUMBLOB allows files up to 16Mb (default for @Lob is BLOB which allows files up to 64Kb)
	@Column(nullable=false, columnDefinition="MEDIUMBLOB")
	private byte[] file;
	
	/**
	 * The score assigned by the professor for the document
	 */
	private int score;
	
	/**
	 * The malus calculated for the document
	 */
	private int malus;
	
	/**
	 * The overall score, calculated considering the assigned scored and the malus
	 */
	private int finalScore;
	
	/**
	 * The date of delivery
	 */
	private Calendar deliveryDate;
		
	/**
	 * The deliverable which the document is referring to
	 */
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="deliverable_id", updatable=false, nullable=false)
	private Deliverable deliverable;
	
	/**
	 * The group that delivered the document
	 */
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="group_id", updatable=false, nullable=false)
	private Group group;
	
	public Document() {}
	

	/**
	 * @param deliverable
	 * @param group
	 * @param file
	 */
	public Document(Deliverable deliverable, Group group, byte[] file) {
		this.file = file;
		this.deliverable = deliverable;
		this.group = group;
		this.deliveryDate = GregorianCalendar.getInstance();
		this.malus = this.calculateMalus();
		this.score = 0;
		this.finalScore = 0;
	}

	/**
	 * @return
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the file
	 */
	public byte[] getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(byte[] file) {
		this.file = file;
	}

	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
		this.finalScore = Math.max(score - this.getMalus(), MIN_SCORE);
	}
	
	/**
	 * @return the malus
	 */
	public int getMalus() {
		return malus;
	}

	/**
	 * @return the finalScore
	 */
	public int getFinalScore() {
		return finalScore;
	}

	/**
	 * @param finalScore the finalScore to set
	 */
	public void setFinalScore(int finalScore) {
		this.finalScore = finalScore;
	}

	/**
	 * @return the deliveryDate
	 */
	public Calendar getDeliveryDate() {
		return deliveryDate;
	}

	/**
	 * @param deliveryDate the deliveryDate to set
	 */
	public void setDeliveryDate(Calendar deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	/**
	 * @return
	 */
	public Deliverable getDeliverable() {
		return deliverable;
	}
	
	/**
	 * @param deliverable
	 */
	public void setDeliverable(Deliverable deliverable) {
		this.deliverable = deliverable;
	}
	
	/**
	 * @return
	 */
	public Group getGroup() {
		return group;
	}
	
	/**
	 * @param group
	 */
	public void setGroup(Group group) {
		this.group = group;
	}
	
	/**
	 * Calculates the malus, which is proportional to the delay of the delivery
	 * @return the malus
	 */
	private int calculateMalus() {
		long deliveryTime = this.getDeliveryDate().getTimeInMillis();
		long dueTime = this.getDeliverable().getDeadline().getTimeInMillis();
		long diff = deliveryTime - dueTime;
		
		if (diff > 0) {
			long secondInMillis = 1000;
			long minuteInMillis = secondInMillis * 60;
			long hourInMillis = minuteInMillis * 60;
			long diffInHours = diff/hourInMillis;
			
			//malus cannot be over MAX_SCORE
			return (int)Math.min(diffInHours/MALUS_RATE_HOURS + 1, MAX_SCORE - MIN_SCORE);
		} else {
			return 0;
		}
	}

	public DocumentDetails asDocumentDetails() {
		DocumentDetails documentDetails = null;
		documentDetails = new DocumentDetails(	this.getId(),
												this.getDeliverable().getId(),
												this.getGroup().getId(), 
												this.getFile(),
												this.getScore(),
												this.getMalus(),
												this.getFinalScore(),
												this.getDeliveryDate());
		return documentDetails;
	}
	
	/**
	 * Two documents are the same if they share the group and the deliverable
	 */
	@Override
	public boolean equals(Object that) {
		if (this == that)
			 return true;
		if (that == null) {
			return false;
		}
		if (!(that instanceof User)) {
			return false;
		}
		Document thatDoc = (Document)that;
		return thatDoc.getGroup().getName().equals(this.getGroup().getName())
			&& thatDoc.getDeliverable().getName().equals(this.getDeliverable().getName());
	}
	
	@Override
	public int hashCode() {
	    int hash = 1;
	    hash = hash * 31 + this.getGroup().getName().hashCode();
	    hash = hash * 31 + this.getDeliverable().getName().hashCode();
	    return hash;
	}
}
