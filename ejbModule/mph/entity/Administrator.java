/**
 * 
 */
package mph.entity;

import javax.persistence.Entity;

/**
 * A system administrator.
 * 
 * @author Gabriele Petronella
 */
@Entity
public class Administrator extends User {
	private static final long serialVersionUID = 1L;
	
	public Administrator() {}
}
