/**
 * 
 */
package mph.exceptions;

import static mph.util.ErrorMessages.INVALID_ARGUMENTS;

/**
 * @author gabrielepetronella
 *
 */
public class InvalidArgumentsException extends Exception {
	
	public InvalidArgumentsException(String string) {
		super(string);
	}

	public InvalidArgumentsException() {
		super(INVALID_ARGUMENTS);
	}
	
	private static final long serialVersionUID = 1L;
	
}
