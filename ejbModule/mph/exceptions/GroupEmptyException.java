package mph.exceptions;

import static mph.util.ErrorMessages.GROUP_EMPTY_EXCEPTION;

public class GroupEmptyException extends Exception {
	private static final long serialVersionUID = 1L;

	public GroupEmptyException() {
		super(GROUP_EMPTY_EXCEPTION);
	}
	
}
