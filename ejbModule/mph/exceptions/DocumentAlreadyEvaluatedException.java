/**
 * 
 */
package mph.exceptions;

import static mph.util.ErrorMessages.DOCUMENT_ALREADY_EVALUATED;

/**
 * @author gabrielepetronella
 *
 */
public class DocumentAlreadyEvaluatedException extends Exception {
	private static final long serialVersionUID = 1L;

	public DocumentAlreadyEvaluatedException() {
		super(DOCUMENT_ALREADY_EVALUATED);
	}
	
}
