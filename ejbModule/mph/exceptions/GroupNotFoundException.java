package mph.exceptions;

import static mph.util.ErrorMessages.GROUP_NOT_FOUND;

public class GroupNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public GroupNotFoundException() {
		super(GROUP_NOT_FOUND);
	}
	
}
