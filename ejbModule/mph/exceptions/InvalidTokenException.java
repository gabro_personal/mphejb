package mph.exceptions;

import static mph.util.ErrorMessages.INVALID_TOKEN;

public class InvalidTokenException extends Exception {
	private static final long serialVersionUID = 1L;

	public InvalidTokenException() {
		super(INVALID_TOKEN);
	}
	
}
