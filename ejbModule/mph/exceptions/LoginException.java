package mph.exceptions;

import static mph.util.ErrorMessages.LOGIN_EXCEPTION;

public class LoginException extends Exception {
	private static final long serialVersionUID = 1L;

	public LoginException() {
		super(LOGIN_EXCEPTION);
	}
	
}
