/**
 * 
 */
package mph.exceptions;

import static mph.util.ErrorMessages.DELIVERABLE_NOT_FOUND;

/**
 * @author gabrielepetronella
 *
 */
public class DeliverableNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public DeliverableNotFoundException() {
		super(DELIVERABLE_NOT_FOUND);
	}
}
