/**
 * 
 */
package mph.exceptions;

import static mph.util.ErrorMessages.USERNAME_TAKEN;

/**
 * @author gabrielepetronella
 *
 */
public class UsernameTakenException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public UsernameTakenException() {
		super(USERNAME_TAKEN);
	}
}
