package mph.exceptions;

import static mph.util.ErrorMessages.STUDENT_NOT_FOUND;

public class StudentNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public StudentNotFoundException() {
		super(STUDENT_NOT_FOUND);
	}
	
}
