/**
 * 
 */
package mph.exceptions;

import static mph.util.ErrorMessages.DOCUMENT_ALREADY_EXISTS_FOR_DELIVERABLE;

/**
 * @author gabrielepetronella
 *
 */
public class DocumentAlreadyExistsForDeliverableException extends Exception {
	private static final long serialVersionUID = 1L;

	public DocumentAlreadyExistsForDeliverableException() {
		super(DOCUMENT_ALREADY_EXISTS_FOR_DELIVERABLE);
	}
	
}
