/**
 * 
 */
package mph.exceptions;

import static mph.util.ErrorMessages.STUDENT_NOT_IN_GROUP;

/**
 * @author gabrielepetronella
 *
 */
public class StudentNotInGroupException extends Exception {
	private static final long serialVersionUID = 1L;

	public StudentNotInGroupException() {
		super(STUDENT_NOT_IN_GROUP);
	}
	
}
