/**
 * 
 */
package mph.exceptions;

import static mph.util.ErrorMessages.INVALID_TOKEN;

/**
 * @author gabrielepetronella
 *
 */
public class InvalidTokenStringException extends Exception {
	private static final long serialVersionUID = 1L;

	public InvalidTokenStringException() {
		super(INVALID_TOKEN);
	}
	
}
