/**
 * 
 */
package mph.exceptions;

import static mph.util.ErrorMessages.STUDENT_IN_MAX_ONE_GROUP_PER_PROJECT;

/**
 * @author gabrielepetronella
 *
 */
public class StudentInMaxOneGroupPerProjectException extends Exception {
	private static final long serialVersionUID = 1L;

	public StudentInMaxOneGroupPerProjectException() {
		super(STUDENT_IN_MAX_ONE_GROUP_PER_PROJECT);
	}
	
}
