package mph.exceptions;

import static mph.util.ErrorMessages.GROUP_LIMIT_REACHED;

public class GroupLimitReachedException extends Exception {
	private static final long serialVersionUID = 1L;

	public GroupLimitReachedException() {
		super(GROUP_LIMIT_REACHED);
	}
	
}
