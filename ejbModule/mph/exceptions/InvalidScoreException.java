package mph.exceptions;

import static mph.util.ErrorMessages.INVALID_SCORE_EXCEPTION;

public class InvalidScoreException extends Exception {
	private static final long serialVersionUID = 1L;

	public InvalidScoreException() {
		super(INVALID_SCORE_EXCEPTION);
	}
}
