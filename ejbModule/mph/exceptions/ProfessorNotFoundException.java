package mph.exceptions;

import static mph.util.ErrorMessages.PROFESSOR_NOT_FOUND;

public class ProfessorNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public ProfessorNotFoundException() {
		super(PROFESSOR_NOT_FOUND);
	}
	
}
