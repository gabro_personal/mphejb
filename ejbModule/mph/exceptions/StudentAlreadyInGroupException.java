/**
 * 
 */
package mph.exceptions;

import static mph.util.ErrorMessages.STUDENT_ALREADY_IN_GROUP;

/**
 * @author gabrielepetronella
 *
 */
public class StudentAlreadyInGroupException extends Exception {
	private static final long serialVersionUID = 1L;

	public StudentAlreadyInGroupException() {
		super(STUDENT_ALREADY_IN_GROUP);
	}
}
