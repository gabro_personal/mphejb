package mph.exceptions;

import static mph.util.ErrorMessages.PROJECT_NOT_FOUND;

public class ProjectNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public ProjectNotFoundException() {
		super(PROJECT_NOT_FOUND);
	}
}
