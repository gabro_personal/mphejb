/**
 * 
 */
package mph.exceptions;

import static mph.util.ErrorMessages.DELIVERABLE_NAME_EXISTS;

/**
 * @author gabrielepetronella
 *
 */
public class DeliverableNameExistsException extends Exception {
	private static final long serialVersionUID = 1L;

	public DeliverableNameExistsException() {
		super(DELIVERABLE_NAME_EXISTS);
	}
}
