/**
 * 
 */
package mph.exceptions;

import static mph.util.ErrorMessages.DOCUMENT_NOT_IN_GROUP;

/**
 * @author gabrielepetronella
 *
 */
public class DocumentNotInGroupException extends Exception {
	private static final long serialVersionUID = 1L;

	public DocumentNotInGroupException() {
		super(DOCUMENT_NOT_IN_GROUP);
	}
}
