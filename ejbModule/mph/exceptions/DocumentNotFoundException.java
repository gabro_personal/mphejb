package mph.exceptions;

import static mph.util.ErrorMessages.DOCUMENT_NOT_FOUND;

public class DocumentNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public DocumentNotFoundException() {
		super(DOCUMENT_NOT_FOUND);
	}
}
