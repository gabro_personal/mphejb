/**
 * 
 */
package mph.remote;

import java.util.Calendar;
import java.util.List;

import javax.ejb.Remote;

import mph.exceptions.DeliverableNameExistsException;
import mph.exceptions.DeliverableNotFoundException;
import mph.exceptions.DocumentNotFoundException;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidArgumentsException;
import mph.exceptions.InvalidFileException;
import mph.exceptions.InvalidScoreException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProfessorNotFoundException;
import mph.exceptions.ProjectNotFoundException;
import mph.exceptions.StudentNotFoundException;
import mph.exceptions.UsernameTakenException;
import mph.util.DeliverableDetails;
import mph.util.DocumentDetails;
import mph.util.GroupDetails;
import mph.util.LoginToken;
import mph.util.ProfessorDetails;
import mph.util.ProjectDetails;
import mph.util.StudentDetails;

/**
 * An interface for a professor
 * 
 * @author Gabriele Petronella
 */
@Remote
public interface ProfessorManagerRemote {

	public DeliverableDetails createDeliverable(LoginToken token, String name, String description, Calendar deadline, int projectId)
			throws InvalidTokenException, ProjectNotFoundException, DeliverableNameExistsException;
	
	/**
	 * Creates a new professor
	 * 
	 * @param token
	 * @param username
	 * @param password
	 * @param firstName
	 * @param lastName
	 * @param avatar
	 * @return the professor details
	 * @throws InvalidTokenException
	 * @throws InvalidFileException
	 * @throws UsernameTakenException
	 * @throws InvalidArgumentsException
	 */
	public ProfessorDetails createProfessor(LoginToken token, String username, String password, String firstName, String lastName, byte[] avatar)
			throws InvalidTokenException, InvalidFileException, UsernameTakenException, InvalidArgumentsException;
	
	/**
	 * Creates a new project
	 * 
	 * @param token
	 * @param name
	 * @param description
	 * @param professorId
	 * @return the project details
	 * @throws InvalidTokenException
	 * @throws ProfessorNotFoundException
	 */
	public ProjectDetails createProject(LoginToken token, String name,
			String description, int professorId) throws InvalidTokenException,
			ProfessorNotFoundException;
	
	/**
	 * Deletes a deliverable
	 * 
	 * @param token
	 * @param deliverableId
	 * @throws InvalidTokenException
	 * @throws DeliverableNotFoundException
	 */
	public void deleteDeliverable(LoginToken token, int deliverableId)
			throws InvalidTokenException, DeliverableNotFoundException;
	
	/**
	 * Deletes a professor
	 * 
	 * @param token
	 * @param professor
	 * @throws InvalidTokenException
	 */
	public void deleteProfessor(LoginToken token, int professorId)
			throws InvalidTokenException;
	
	/**
	 * Assigns a score to a document
	 * 
	 * @param token
	 * @param documentId
	 * @param score
	 * @return the document details
	 * @throws InvalidTokenException
	 * @throws DocumentNotFoundException
	 * @throws InvalidScoreException
	 */
	public DocumentDetails evaluateDocument(LoginToken token, int documentId, int score)
			throws InvalidTokenException, DocumentNotFoundException, InvalidScoreException;
	
	/**
	 * Gets a deliverable details
	 * 
	 * @param token
	 * @param deliverableId
	 * @return the deliverable details
	 * @throws InvalidTokenException
	 * @throws DeliverableNotFoundException
	 */
	public DeliverableDetails getDeliverable(LoginToken token, int deliverableId)
			throws InvalidTokenException, DeliverableNotFoundException;
	
	/**
	 * Gets a document details
	 * 
	 * @param token
	 * @param documentId
	 * @return the document details
	 * @throws InvalidTokenException
	 * @throws DocumentNotFoundException
	 */
	public DocumentDetails getDocument(LoginToken token, int documentId)
			throws InvalidTokenException, DocumentNotFoundException;
	
	public GroupDetails getGroup(LoginToken token, int groupId)
			throws InvalidTokenException, GroupNotFoundException;
	
	/**
	 * Gets a professor details
	 * 
	 * @param token
	 * @param professorId
	 * @return the professor details
	 * @throws InvalidTokenException
	 * @throws ProfessorNotFoundException
	 */
	public ProfessorDetails getProfessor(LoginToken token, int professorId)
			throws InvalidTokenException, ProfessorNotFoundException;
	
	/**
	 * Gets a project details
	 * 
	 * @param token
	 * @param projectId
	 * @return the project details
	 * @throws InvalidTokenException
	 * @throws ProjectNotFoundException
	 */
	public ProjectDetails getProject(LoginToken token, int projectId)
			throws InvalidTokenException, ProjectNotFoundException;
	
	/**
	 * Gets all the projects existing in the system
	 * 
	 * @param token
	 * @param professor
	 * @return all the projects details
	 * @throws InvalidTokenException
	 * @throws ProfessorNotFoundException 
	 */
	public List<ProjectDetails> getProjectsForProfessor(LoginToken token, int professorId)
			throws InvalidTokenException, ProfessorNotFoundException;
	
	/**
	 * Updates a deliverable. Passing a null value as parameter causes that field not to be updated
	 * @param token
	 * @param deliverableId
	 * @param name
	 * @param description
	 * @param deadline
	 * @return
	 * @throws InvalidTokenException
	 * @throws DeliverableNotFoundException
	 * @throws DeliverableNameExistsException
	 */
	public DeliverableDetails updateDeliverable(LoginToken token, int deliverableId, String name, String description, Calendar deadline)
			throws InvalidTokenException, DeliverableNotFoundException, DeliverableNameExistsException;
	
	/**
	 * Updates a professor. Passing a null value as parameter causes that field not to be updated
	 * 
	 * @param token
	 * @param professor
	 * @return
	 * @throws InvalidTokenException
	 * @throws InvalidFileException 
	 */
	public ProfessorDetails updateProfessor(LoginToken token, int professorId, String username, String password, String firstName, String lastName, byte[] avatar)
			throws InvalidTokenException, InvalidFileException;
	
	/**
	 * Updates a project. Passing a null value as parameter causes that field not to be updated
	 * 
	 * @param token
	 * @param projectId
	 * @param name
	 * @param description
	 * @return
	 * @throws InvalidTokenException
	 * @throws ProjectNotFoundException
	 */
	public ProjectDetails updateProject(LoginToken token, int projectId, String name, String description)
			throws InvalidTokenException, ProjectNotFoundException;
	
	/**
	 * Get all the professors existing in the system
	 * 
	 * @param token
	 * @return a list of all the professors details
	 * @throws InvalidTokenException
	 */
	public List<ProfessorDetails> getAllProfessors(LoginToken token)
			throws InvalidTokenException;
	
	/**
	 * Get a student details
	 * 
	 * @param token
	 * @param studentId
	 * @return the student details
	 * @throws InvalidTokenException
	 * @throws StudentNotFoundException
	 */
	public StudentDetails getStudent(LoginToken token, int studentId) throws InvalidTokenException, StudentNotFoundException;
}
