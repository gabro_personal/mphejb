package mph.remote;
import java.util.List;

import javax.ejb.Remote;

import mph.exceptions.InvalidArgumentsException;
import mph.exceptions.InvalidFileException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProfessorNotFoundException;
import mph.exceptions.UsernameTakenException;
import mph.util.LoginToken;
import mph.util.ProfessorDetails;

/**
 * An interface for the system adminstrator
 * 
 * @author Gabriele
 */
@Remote
public interface AdministratorManagerRemote {
	
	
	/**
	 * Creates a new professor
	 * 
	 * @param token
	 * @param username
	 * @param password
	 * @param firstName
	 * @param lastName
	 * @param avatar
	 * @return the professor details
	 * @throws InvalidTokenException
	 * @throws InvalidFileException
	 * @throws UsernameTakenException
	 * @throws InvalidArgumentsException
	 */
	public ProfessorDetails createProfessor(LoginToken token, String username, String password, String firstName, String lastName, byte[] avatar)
			throws InvalidTokenException, InvalidFileException, UsernameTakenException, InvalidArgumentsException;
	
	/**
	 * Deletes a professor
	 * 
	 * @param token
	 * @param professorId
	 * @throws InvalidTokenException
	 */
	public void deleteProfessor(LoginToken token, int professorId)
			throws InvalidTokenException;
	

	/**
	 * Updates a professor. Passing a null value as parameter causes that field not to be updated
	 * 
	 * @param token
	 * @param professorId
	 * @param username
	 * @param password
	 * @param firstName
	 * @param lastName
	 * @param avatar
	 * @return the professor details
	 * @throws InvalidTokenException
	 * @throws InvalidFileException
	 */
	public ProfessorDetails updateProfessor(LoginToken token, int professorId, String username, String password, String firstName, String lastName, byte[] avatar)
			throws InvalidTokenException, InvalidFileException;
	
	/**
	 * Get professor details
	 * 
	 * @param token
	 * @param professorId
	 * @return the professor details
	 * @throws InvalidTokenException
	 * @throws ProfessorNotFoundException
	 */
	public ProfessorDetails getProfessor(LoginToken token, int professorId)
			throws InvalidTokenException, ProfessorNotFoundException;

	/**
	 * Get all the professors existing in the system
	 * 
	 * @param token
	 * @return a list of all the professors details
	 * @throws InvalidTokenException
	 */
	public List<ProfessorDetails> getAllProfessors(LoginToken token) throws InvalidTokenException;
	
}
