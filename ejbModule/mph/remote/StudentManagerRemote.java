package mph.remote;

import java.util.List;

import javax.ejb.Remote;

import mph.exceptions.DeliverableNotFoundException;
import mph.exceptions.DocumentAlreadyEvaluatedException;
import mph.exceptions.DocumentAlreadyExistsForDeliverableException;
import mph.exceptions.DocumentNotFoundException;
import mph.exceptions.GroupEmptyException;
import mph.exceptions.GroupLimitReachedException;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidArgumentsException;
import mph.exceptions.InvalidFileException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProjectNotFoundException;
import mph.exceptions.StudentAlreadyInGroupException;
import mph.exceptions.StudentInMaxOneGroupPerProjectException;
import mph.exceptions.StudentNotFoundException;
import mph.exceptions.StudentNotInGroupException;
import mph.exceptions.UsernameTakenException;
import mph.util.DeliverableDetails;
import mph.util.DocumentDetails;
import mph.util.GroupDetails;
import mph.util.LoginToken;
import mph.util.ProjectDetails;
import mph.util.StudentDetails;


/**
 * An interface for a student
 * 
 * @author Gabriele Petronella
 */
@Remote
public interface StudentManagerRemote {

	/**
	 * Adds a Student to a Group
	 * 
	 * @param token
	 * @param studentId
	 * @param groupId
	 * @throws InvalidTokenException
	 * @throws StudentNotFoundException
	 * @throws GroupNotFoundException
	 * @throws GroupLimitReachedException
	 * @throws StudentInMaxOneGroupPerProjectException
	 * @throws StudentAlreadyInGroupException
	 */
	public void addStudentToGroup(LoginToken token, int studentId, int groupId)
			throws InvalidTokenException, StudentNotFoundException, GroupNotFoundException, GroupLimitReachedException, StudentInMaxOneGroupPerProjectException, StudentAlreadyInGroupException;
	
	/**
	 * Creates a document
	 * 
	 * @param token
	 * @param file
	 * @param groupId
	 * @param deliverableId
	 * @throws InvalidTokenException
	 * @throws InvalidFileException
	 * @throws GroupNotFoundException
	 * @throws DeliverableNotFoundException
	 * @throws DocumentAlreadyExistsForDeliverableException
	 */
	public void createDocument(LoginToken token, byte[] file, int groupId, int deliverableId) 
			throws InvalidTokenException, InvalidFileException, GroupNotFoundException, DeliverableNotFoundException, DocumentAlreadyExistsForDeliverableException;

	
	/**
	 * Crates a group, assigning it to an existing project
	 * 
	 * @param token
	 * @param name
	 * @param projectId
	 * @return the group details
	 * @throws InvalidTokenException
	 * @throws ProjectNotFoundException
	 */
	public GroupDetails createGroup(LoginToken token, String name, int projectId)
			throws InvalidTokenException, ProjectNotFoundException;

	/**
	 * Creates a student
	 * 
	 * @param student
	 * @return the student details
	 * @throws UsernameTakenException 
	 * @throws InvalidArgumentsException 
	 * @throws InvalidFileException 
	 */
	public StudentDetails createStudent(String username, String password, String firstName, String lastName, String matrNo, byte[] avatar) 
			throws UsernameTakenException, InvalidArgumentsException, InvalidFileException;
	
	/**
	 * Deletes a document
	 * 
	 * @param token
	 * @param documentId
	 * @throws InvalidTokenException
	 * @throws DocumentNotFoundException
	 */
	public void deleteDocument(LoginToken token, int documentId)
			throws InvalidTokenException, DocumentNotFoundException;
	

	/**
	 * Deletes a student
	 * 
	 * @param token
	 * @param student
	 * @throws InvalidTokenException
	 */
	public void deleteStudent(LoginToken token, int studentId) throws InvalidTokenException, StudentNotFoundException;
	
	/**
	 * @return
	 * @throws InvalidTokenException 
	 */
	public List<ProjectDetails> getAllProjects(LoginToken token) throws InvalidTokenException;
	
	/**
	 * @param token
	 * @return
	 * @throws InvalidTokenException
	 */
	public List<StudentDetails> getAllStudents(LoginToken token) throws InvalidTokenException;
	
	public DeliverableDetails getDeliverable(LoginToken token, int deliverableId)
			throws InvalidTokenException, DeliverableNotFoundException;
	
	/**
	 * @param token
	 * @param projectId
	 * @return
	 * @throws InvalidTokenException
	 * @throws ProjectNotFoundException
	 */
	public List<DeliverableDetails> getDeliverablesForProject(LoginToken token, int projectId) 
			throws InvalidTokenException, ProjectNotFoundException;
	
	public DocumentDetails getDocument(LoginToken token, int documentId)
			throws InvalidTokenException, DocumentNotFoundException;

	public GroupDetails getGroup(LoginToken token, int groupId)
			throws InvalidTokenException, GroupNotFoundException;
	
	/**
	 * @param projectId
	 * @return
	 * @throws ProjectNotFoundException
	 */
	public List<GroupDetails> getGroupsForProject(LoginToken token, int projectId)
			throws InvalidTokenException, ProjectNotFoundException;
	
	public List<GroupDetails> getGroupsForStudent(LoginToken token, int studentId)
			throws InvalidTokenException, StudentNotFoundException;
	
	/**
	 * @param token
	 * @param projectId
	 * @return
	 * @throws InvalidTokenException
	 * @throws ProjectNotFoundException
	 */
	public ProjectDetails getProject(LoginToken token, int projectId) 
			throws InvalidTokenException, ProjectNotFoundException;
	
	/**
	 * @param token
	 * @param studentId
	 * @return
	 * @throws InvalidTokenException
	 * @throws StudentNotFoundException
	 */
	public StudentDetails getStudent(LoginToken token, int studentId) throws InvalidTokenException, StudentNotFoundException;
	
	/**
	 * @param token
	 * @param groupId
	 * @return
	 * @throws InvalidTokenException
	 * @throws GroupNotFoundException
	 */
	public List<StudentDetails> getStudentsForGroup(LoginToken token, int groupId) throws InvalidTokenException, GroupNotFoundException;
		
	/**
	 * @param token
	 * @param studentId
	 * @param groupId
	 * @throws StudentNotFoundException
	 * @throws GroupNotFoundException
	 * @throws GroupEmptyException
	 * @throws StudentNotInGroupException 
	 */
	public void removeStudentFromGroup(LoginToken token, int studentId, int groupId) throws InvalidTokenException, StudentNotFoundException, GroupNotFoundException, GroupEmptyException, StudentNotInGroupException;
	
	/**
	 * @param token
	 * @param documentId
	 * @param file
	 * @throws InvalidTokenException
	 * @throws DocumentNotFoundException
	 * @throws InvalidFileException
	 * @throws DocumentAlreadyEvaluatedException
	 */
	public void updateDocument(LoginToken token, int documentId, byte[] file)
			throws InvalidTokenException, DocumentNotFoundException, InvalidFileException, DocumentAlreadyEvaluatedException;
	
	/**
	 * @param token
	 * @param student
	 * @return
	 * @throws InvalidTokenException
	 * @throws StudentNotFoundException
	 * @throws InvalidFileException 
	 */
	public StudentDetails updateStudent(LoginToken token, int studentId, String username, String password, String firstName, String lastName, String matrNo, byte[] avatar) throws InvalidTokenException, StudentNotFoundException, InvalidFileException;
}
