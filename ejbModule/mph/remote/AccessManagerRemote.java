/**
 * 
 */
package mph.remote;

import javax.ejb.Remote;

import mph.exceptions.InvalidTokenException;
import mph.exceptions.LoginException;
import mph.util.LoginToken;
import mph.util.UserType;

/**
 * The controller responsible of the authentication to the system
 * 
 * @author Gabriele Petronella
 */
@Remote
public interface AccessManagerRemote {
	/**
	 * Logs the user in
	 * @param username the username of the user
	 * @param password the password of the user
	 * @return a unique LoginToken needed to call authenticated methods
	 * @throws LoginException generic login exception (e.g. login info not valid)
	 */
	public LoginToken login(String username, String password) throws LoginException;
	
	/**
	 * Ends the session for the user identified by the token
	 * @param token a valid token for the user
	 */
	public void logout(LoginToken token);
	
	/**
	 * Identify the user as Student, Professor or Administrator
	 * @param token
	 * @return the UserType
	 * @throws InvalidTokenException
	 */
	public UserType getUserType(LoginToken token) throws InvalidTokenException; 
}
