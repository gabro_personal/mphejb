package mph.local;
import javax.ejb.Local;

import mph.exceptions.DeliverableNotFoundException;
import mph.exceptions.DocumentAlreadyEvaluatedException;
import mph.exceptions.DocumentAlreadyExistsForDeliverableException;
import mph.exceptions.DocumentNotFoundException;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidFileException;
import mph.exceptions.InvalidScoreException;
import mph.exceptions.InvalidTokenException;
import mph.util.DocumentDetails;
import mph.util.LoginToken;

@Local
public interface DocumentManagerLocal {

	/**
	 * @param token
	 * @param deliverableId
	 * @param groupId
	 * @param file
	 * @return
	 * @throws InvalidTokenException
	 * @throws DeliverableNotFoundException
	 * @throws GroupNotFoundException
	 * @throws InvalidFileException
	 * @throws DocumentAlreadyExistsForDeliverableException 
	 */
	public DocumentDetails createDocument(LoginToken token, int deliverableId, int groupId, byte[] file)
			throws InvalidTokenException, DeliverableNotFoundException, GroupNotFoundException, InvalidFileException, DocumentAlreadyExistsForDeliverableException;
	
	/**
	 * @param token
	 * @param documentId
	 * @return
	 * @throws InvalidTokenException
	 * @throws DocumentNotFoundException
	 */
	public void deleteDocument(LoginToken token, int documentId)
			throws InvalidTokenException, DocumentNotFoundException;
	
	/**
	 * @param token
	 * @param documentId
	 * @return
	 * @throws InvalidTokenException
	 * @throws DocumentNotFoundException
	 */
	public DocumentDetails evaluateDocument(LoginToken token, int documentId, int score)
			throws InvalidTokenException, DocumentNotFoundException, InvalidScoreException;

	DocumentDetails findDocument(LoginToken token, int documentId)
			throws InvalidTokenException, DocumentNotFoundException;
	
	DocumentDetails updateDocument(LoginToken token, int documentId, byte[] file)
			throws InvalidTokenException, DocumentNotFoundException, InvalidFileException, DocumentAlreadyEvaluatedException;
}
