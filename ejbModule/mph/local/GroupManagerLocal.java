package mph.local;
import javax.ejb.Local;

import mph.exceptions.GroupEmptyException;
import mph.exceptions.GroupLimitReachedException;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProjectNotFoundException;
import mph.exceptions.StudentAlreadyInGroupException;
import mph.exceptions.StudentInMaxOneGroupPerProjectException;
import mph.exceptions.StudentNotFoundException;
import mph.exceptions.StudentNotInGroupException;
import mph.util.GroupDetails;
import mph.util.LoginToken;

@Local
public interface GroupManagerLocal {
	
	/**
	 * @param token
	 * @return
	 * @throws InvalidTokenException
	 */
	public GroupDetails createGroup(LoginToken token, String name, int projectId)
			throws InvalidTokenException, ProjectNotFoundException;
	
	/**
	 * @param token
	 * @param groupId
	 * @return
	 * @throws InvalidTokenException
	 * @throws GroupNotFoundException
	 */
	public GroupDetails findGroup(LoginToken token, int groupId)
			throws InvalidTokenException, GroupNotFoundException;

	/**
	 * @param token
	 * @param groupId
	 * @param studentId
	 * @return
	 * @throws InvalidTokenException
	 * @throws GroupNotFoundException
	 * @throws StudentNotFoundException
	 * @throws GroupLimitReachedException
	 * @throws StudentInMaxOneGroupPerProjectException 
	 * @throws StudentAlreadyInGroupException 
	 */
	public GroupDetails addStudentToGroup(LoginToken token, int groupId, int studentId)
			throws InvalidTokenException, GroupNotFoundException, StudentNotFoundException, GroupLimitReachedException, StudentInMaxOneGroupPerProjectException, StudentAlreadyInGroupException;
	
	
	/**
	 * @param token
	 * @param groupId
	 * @param studentId
	 * @return
	 * @throws InvalidTokenException
	 * @throws GroupNotFoundException
	 * @throws StudentNotFoundException
	 * @throws StudentNotInGroupException
	 * @throws GroupEmptyException
	 */
	public GroupDetails removeStudentFromGroup(LoginToken token, int groupId, int studentId)
			throws InvalidTokenException, GroupNotFoundException, StudentNotFoundException, StudentNotInGroupException, GroupEmptyException;
}
