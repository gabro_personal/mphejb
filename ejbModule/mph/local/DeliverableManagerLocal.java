package mph.local;
import java.util.Calendar;

import javax.ejb.Local;

import mph.exceptions.DeliverableNameExistsException;
import mph.exceptions.DeliverableNotFoundException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProjectNotFoundException;
import mph.util.DeliverableDetails;
import mph.util.LoginToken;

@Local
public interface DeliverableManagerLocal {

	/**
	 * @param token
	 * @param name
	 * @param description
	 * @param deadline
	 * @param projectId
	 * @return
	 * @throws InvalidTokenException
	 * @throws ProjectNotFoundException
	 * @throws DeliverableNameExistsException
	 */
	public DeliverableDetails createDeliverable(LoginToken token, String name, String description, Calendar deadline, int projectId)
			throws InvalidTokenException, ProjectNotFoundException, DeliverableNameExistsException;
	
	/**
	 * @param token
	 * @param deliverableId
	 * @return
	 * @throws InvalidTokenException
	 * @throws DeliverableNotFoundException
	 */
	public DeliverableDetails findDeliverable(LoginToken token, int deliverableId)
			throws InvalidTokenException, DeliverableNotFoundException;
	
	/**
	 * @param token
	 * @param deliverableId
	 * @param name
	 * @param description
	 * @param deadline
	 * @return
	 * @throws InvalidTokenException
	 * @throws DeliverableNotFoundException
	 * @throws DeliverableNameExistsException
	 */
	public DeliverableDetails updateDeliverable(LoginToken token, int deliverableId, String name, String description, Calendar deadline)
			throws InvalidTokenException, DeliverableNotFoundException, DeliverableNameExistsException;
	
	/**
	 * @param token
	 * @param deliverableId
	 * @throws InvalidTokenException
	 * @throws DeliverableNotFoundException
	 */
	public void deleteDeliverable(LoginToken token, int deliverableId)
			throws InvalidTokenException, DeliverableNotFoundException;
}
