package mph.local;
import javax.ejb.Local;

import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProfessorNotFoundException;
import mph.exceptions.ProjectNotFoundException;
import mph.util.LoginToken;
import mph.util.ProjectDetails;

@Local
public interface ProjectManagerLocal {

	/**
	 * @param token
	 * @param name
	 * @param description
	 * @param professor
	 * @return
	 * @throws InvalidTokenException
	 * @throws ProfessorNotFoundException
	 */
	public ProjectDetails createProject(LoginToken token, String name, String description, int professorId)
			throws InvalidTokenException, ProfessorNotFoundException;
	
	public ProjectDetails findProject(LoginToken token, int projectId)
			throws InvalidTokenException, ProjectNotFoundException;	
	
	public ProjectDetails updateProject(LoginToken token, int projectId, String name, String description)
			throws InvalidTokenException, ProjectNotFoundException;
}
