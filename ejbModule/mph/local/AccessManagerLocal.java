package mph.local;
import javax.ejb.Local;

import mph.entity.LoginSession;
import mph.entity.User;
import mph.exceptions.InvalidTokenException;
import mph.util.LoginToken;
import mph.util.UserType;

/**
 * @author Gabriele
 *
 */
@Local
public interface AccessManagerLocal {
	
	/**
	 * Checks the validity of the token
	 * @param token
	 * @return
	 * @throws NotLoggedException 
	 */
	public void checkUserLogin(LoginToken token) throws InvalidTokenException;
	
	/**
	 * Retrieves the current session for the user -if any- or creates a new one
	 * @param user
	 * @return
	 */
	public LoginSession getUserSession(User user);

	/**
	 * Checks the token validity
	 * @param token
	 * @return
	 */
	public boolean isValidToken(LoginToken token);
	
	public UserType getUserType(LoginToken token) throws InvalidTokenException; 
}
