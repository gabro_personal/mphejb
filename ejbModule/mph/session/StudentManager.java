package mph.session;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import mph.entity.Deliverable;
import mph.entity.Group;
import mph.entity.Project;
import mph.entity.Student;
import mph.exceptions.DeliverableNotFoundException;
import mph.exceptions.DocumentAlreadyEvaluatedException;
import mph.exceptions.DocumentAlreadyExistsForDeliverableException;
import mph.exceptions.DocumentNotFoundException;
import mph.exceptions.GroupEmptyException;
import mph.exceptions.GroupLimitReachedException;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidArgumentsException;
import mph.exceptions.InvalidFileException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProjectNotFoundException;
import mph.exceptions.StudentAlreadyInGroupException;
import mph.exceptions.StudentInMaxOneGroupPerProjectException;
import mph.exceptions.StudentNotFoundException;
import mph.exceptions.StudentNotInGroupException;
import mph.exceptions.UsernameTakenException;
import mph.remote.StudentManagerRemote;
import mph.util.DeliverableDetails;
import mph.util.DocumentDetails;
import mph.util.FieldValidator;
import mph.util.GroupDetails;
import mph.util.LoginToken;
import mph.util.ProjectDetails;
import mph.util.SecureManager;
import mph.util.StudentDetails;

import org.hibernate.exception.ConstraintViolationException;
import org.jboss.ejb3.annotation.RemoteBinding;

/**
 * Session Bean implementation class StudentManager
 */
@Stateless
@RemoteBinding(jndiBinding="StudentManagerJNDI")
public class StudentManager extends SecureManager implements StudentManagerRemote {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void addStudentToGroup(LoginToken token, int studentId, int groupId)
			throws InvalidTokenException, StudentNotFoundException, GroupNotFoundException,
			GroupLimitReachedException, StudentInMaxOneGroupPerProjectException, StudentAlreadyInGroupException {
		this.getGroupManager().addStudentToGroup(token, groupId, studentId);
	}

	@Override
	public void createDocument(LoginToken token, byte[] file, int groupId,
			int deliverableId) throws InvalidTokenException,
			InvalidFileException, GroupNotFoundException,
			DeliverableNotFoundException, DocumentAlreadyExistsForDeliverableException {
		this.getDocumentManager().createDocument(token, deliverableId, groupId, file);
	}

	@Override
	public GroupDetails createGroup(LoginToken token, String name, int projectId)
			throws InvalidTokenException, ProjectNotFoundException {
		return this.getGroupManager().createGroup(token, name, projectId);
	}

	@Override
	public StudentDetails createStudent(String username, String password,
			String firstName, String lastName, String matrNo, byte[] avatar) throws UsernameTakenException, InvalidArgumentsException, InvalidFileException {
		//Validate fields
		if (!FieldValidator.isUsernameValid(username)) {
			throw new InvalidArgumentsException("Invalid username");
		}
		if (!FieldValidator.isPasswordValid(password)) {
			throw new InvalidArgumentsException("Invalid password");
		}
		if (!FieldValidator.isFirstNameValid(firstName)) {
			throw new InvalidArgumentsException("Invalid first name");
		}
		if (!FieldValidator.isLastNameValid(lastName)) {
			throw new InvalidArgumentsException("Invalid last name");
		}
		if (!FieldValidator.isMatriculationNoValid(matrNo)) {
			throw new InvalidArgumentsException("Invalid matrNo");
		}
		
		//TODO Check mime type	
		
		Student student = new Student(username, password, firstName, lastName, matrNo, avatar);
		try {
			entityManager.persist(student);
		} catch (PersistenceException e) {
			//Username has a 'unique' constraint
			if(e.getCause() instanceof ConstraintViolationException) {
				throw new UsernameTakenException();
			}
		}
		System.out.println("Created new student");
		return student.asStudentDetails();
	}

	@Override
	public void deleteDocument(LoginToken token, int documentId)
			throws InvalidTokenException, DocumentNotFoundException {
		this.getDocumentManager().deleteDocument(token, documentId);
	}

	@Override
	public void deleteStudent(LoginToken token, int studentId)
			throws InvalidTokenException, StudentNotFoundException {
		this.getAccessManager().checkUserLogin(token);
		//A user can delete only himself
		if (token.getUserID() != studentId) {
			throw new InvalidTokenException();
		}
		//Pattern seek and destroy: retrieve the entity before you can delete it
		Student s = entityManager.find(Student.class, studentId);
		if(s == null) {
			throw new StudentNotFoundException();
		}
		entityManager.remove(s);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProjectDetails> getAllProjects(LoginToken token) throws InvalidTokenException {
		this.getAccessManager().checkUserLogin(token);
		Query q = this.entityManager.createQuery("SELECT OBJECT(p) FROM Project p");
		List<Project> projects = (List<Project>)q.getResultList();
		List<ProjectDetails> projectsDetails = new ArrayList<ProjectDetails>();
		for (Project project : projects) {
			projectsDetails.add(project.asProjectDetails());
		}
		return projectsDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StudentDetails> getAllStudents(LoginToken token)
			throws InvalidTokenException {
		//Check the token (this will eventually throw an exception)
		this.getAccessManager().checkUserLogin(token);

		Query q = this.entityManager.createQuery("SELECT OBJECT(u) FROM User u WHERE UserType='Student'");
		
		ArrayList<StudentDetails> studentsDetails = new ArrayList<StudentDetails>();
		List<Student> students = (List<Student>)q.getResultList();
		for (Student student : students) {
			studentsDetails.add(student.asStudentDetails());
		}
		return studentsDetails;
	}

	@Override
	public DeliverableDetails getDeliverable(LoginToken token, int deliverableId)
			throws InvalidTokenException, DeliverableNotFoundException {
		return this.getDeliverableManager().findDeliverable(token, deliverableId);
	}

	@Override
	public List<DeliverableDetails> getDeliverablesForProject(LoginToken token,
			int projectId) throws InvalidTokenException,
			ProjectNotFoundException {
		this.getAccessManager().checkUserLogin(token);
		Project p = entityManager.find(Project.class, projectId);
		if (p == null) {
			throw new ProjectNotFoundException();
		}
		List<DeliverableDetails> deliverablesDetails = new ArrayList<DeliverableDetails>();
		if (p.getDeliverables() != null) {
			for (Deliverable d : p.getDeliverables()) {
				deliverablesDetails.add(d.asDeliverableDetails());
			}
		}
		return deliverablesDetails;
	}

	@Override
	public DocumentDetails getDocument(LoginToken token, int documentId)
			throws InvalidTokenException, DocumentNotFoundException {
		return this.getDocumentManager().findDocument(token, documentId);
	}

	@Override
	public GroupDetails getGroup(LoginToken token, int groupId)
			throws InvalidTokenException, GroupNotFoundException {
		return this.getGroupManager().findGroup(token, groupId);
	}

	@Override
	public List<GroupDetails> getGroupsForProject(LoginToken token, int projectId)
			throws InvalidTokenException, ProjectNotFoundException {
		this.getAccessManager().checkUserLogin(token);
		Project p = entityManager.find(Project.class, projectId);
		if (p == null) {
			throw new ProjectNotFoundException();
		}
		ArrayList<GroupDetails> groupsDetails = new ArrayList<GroupDetails>();
		if (p.getGroups() != null) {
			for (Group g : p.getGroups()) {
				groupsDetails.add(g.asGroupDetails());
			}
		}
		return groupsDetails;
	}

	@Override
	public List<GroupDetails> getGroupsForStudent(LoginToken token,
			int studentId) throws InvalidTokenException,
			StudentNotFoundException {
		this.getAccessManager().checkUserLogin(token);
		Student s = entityManager.find(Student.class, studentId);
		if (s == null) {
			throw new StudentNotFoundException();
		}
		List<GroupDetails> groupsDetails = new ArrayList<GroupDetails>();
		if(s.getGroups() != null) {
			for (Group g : s.getGroups()) {
				groupsDetails.add(g.asGroupDetails());
			}
		}
		return groupsDetails;
	}

	@Override
	public ProjectDetails getProject(LoginToken token, int projectId)
			throws InvalidTokenException, ProjectNotFoundException {
		return this.getProjectManager().findProject(token, projectId);
	}

	@Override
	public StudentDetails getStudent(LoginToken token, int studentId)
			throws InvalidTokenException, StudentNotFoundException {
		//Check the token (this will eventually throw an exception)
		this.getAccessManager().checkUserLogin(token);
		Student s = entityManager.find(Student.class, studentId);
		if(s == null) {
			throw new StudentNotFoundException();
		}
		return s.asStudentDetails();
	}

	@Override
	public List<StudentDetails> getStudentsForGroup(LoginToken token, int groupId)
			throws InvalidTokenException, GroupNotFoundException {
		//Check the token (this will eventually throw an exception)
		this.getAccessManager().checkUserLogin(token);
		
		//Retrieve the group
		Group group = entityManager.find(Group.class, groupId);
		if (group == null) {
			throw new GroupNotFoundException();
		}
		
		//Return the array of details
		ArrayList<StudentDetails> studentsDetails = new ArrayList<StudentDetails>();
		for (Student s : group.getMembers()) { //Iterate over the group members
			studentsDetails.add(s.asStudentDetails());
		}
		return studentsDetails;
	}


	@Override
	public void removeStudentFromGroup(LoginToken token, int studentId,
			int groupId) throws InvalidTokenException,
			StudentNotFoundException, GroupNotFoundException,
			GroupEmptyException, StudentNotInGroupException {
		this.getGroupManager().removeStudentFromGroup(token, groupId, studentId);
	}

	@Override
	public void updateDocument(LoginToken token, int documentId, byte[] file)
			throws InvalidTokenException, DocumentNotFoundException,
			InvalidFileException, DocumentAlreadyEvaluatedException {
		this.getDocumentManager().updateDocument(token, documentId, file);
	}

	@Override
	public StudentDetails updateStudent(LoginToken token, int studentId, String username,
			String password, String firstName, String lastName, String matrNo, byte[] avatar)
			throws InvalidTokenException, StudentNotFoundException, InvalidFileException {
		this.getAccessManager().checkUserLogin(token);
		if (token.getUserID() != studentId) {
			throw new InvalidTokenException();
		}
		Student s = entityManager.find(Student.class, studentId);
		if(s == null) {
			throw new StudentNotFoundException();
		}
		
		//Modify only non-null fields
		if (username != null) {
			s.setUsername(username);
		}
		if (password != null) {
			s.setPassword(password);
		}
		if (firstName != null) {
			s.setFirstName(firstName);
		}
		if (lastName != null) {
			s.setLastName(lastName);
		}
		if (matrNo != null) {
			s.setMatrNo(matrNo);
		}
		if (avatar != null) {
			s.setAvatarData(avatar);
		}
		
		entityManager.merge(s);
		return s.asStudentDetails();
	}
}
