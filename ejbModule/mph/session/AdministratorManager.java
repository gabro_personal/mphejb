package mph.session;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.ejb3.annotation.RemoteBinding;

import mph.exceptions.InvalidArgumentsException;
import mph.exceptions.InvalidFileException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProfessorNotFoundException;
import mph.exceptions.UsernameTakenException;
import mph.remote.AdministratorManagerRemote;
import mph.util.LoginToken;
import mph.util.ProfessorDetails;
import mph.util.SecureManager;

/**
 * Session Bean implementation class AdministratorManager
 */
@Stateless
@RemoteBinding(jndiBinding="AdministratorManagerJNDI")
public class AdministratorManager extends SecureManager implements AdministratorManagerRemote {

	@PersistenceContext
	EntityManager entityManager;

	@Override
	public ProfessorDetails createProfessor(LoginToken token, String username,
			String password, String firstName, String lastName, byte[] avatar)
			throws InvalidTokenException, InvalidFileException, UsernameTakenException, InvalidArgumentsException {
		return this.getProfessorManager().createProfessor(token, username, password, firstName, lastName, avatar);
	}

	@Override
	public void deleteProfessor(LoginToken token, int professorId)
			throws InvalidTokenException {
		this.getProfessorManager().deleteProfessor(token, professorId);
	}

	@Override
	public ProfessorDetails updateProfessor(LoginToken token, int professorId,
			String username, String password, String firstName,
			String lastName, byte[] avatar) throws InvalidTokenException,
			InvalidFileException {
		return this.getProfessorManager().updateProfessor(token, professorId, username, password, firstName, lastName, avatar);
	}

	@Override
	public ProfessorDetails getProfessor(LoginToken token, int professorId)
			throws InvalidTokenException, ProfessorNotFoundException {
		return this.getProfessorManager().getProfessor(token, professorId);
	}

	@Override
	public List<ProfessorDetails> getAllProfessors(LoginToken token)
			throws InvalidTokenException {
		return this.getProfessorManager().getAllProfessors(token);
	}
	
	

}
