package mph.session;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mph.entity.Deliverable;
import mph.entity.Professor;
import mph.entity.Project;
import mph.entity.Student;
import mph.entity.User;
import mph.exceptions.DeliverableNameExistsException;
import mph.exceptions.DeliverableNotFoundException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProjectNotFoundException;
import mph.local.DeliverableManagerLocal;
import mph.util.DeliverableDetails;
import mph.util.LoginToken;
import mph.util.SecureManager;

import org.jboss.ejb3.annotation.LocalBinding;

/**
 * Session Bean implementation class DeliverableManager
 */
@Stateless
@LocalBinding(jndiBinding="DeliverableManagerJNDI")
public class DeliverableManager extends SecureManager implements DeliverableManagerLocal {

    @PersistenceContext
    EntityManager entityManager;

	@Override
	public DeliverableDetails createDeliverable(LoginToken token, String name,
			String description, Calendar deadline, int projectId)
			throws InvalidTokenException, ProjectNotFoundException,
			DeliverableNameExistsException {
		this.getAccessManager().checkUserLogin(token);
		Project p = entityManager.find(Project.class, projectId);
		if (p == null) {
			throw new ProjectNotFoundException();
		}
		//Only the project owner can create a deliverable
		if (p.getProfessor().getId() != token.getUserID()) {
			throw new InvalidTokenException();
		}
		Deliverable d = new Deliverable(p, name, description, deadline);
		entityManager.persist(d);

		return d.asDeliverableDetails();
	}

	@Override
	public DeliverableDetails findDeliverable(LoginToken token,
			int deliverableId) throws InvalidTokenException,
			DeliverableNotFoundException {
		this.getAccessManager().checkUserLogin(token);
		Deliverable d = entityManager.find(Deliverable.class, deliverableId);
		if (d == null) {
			throw new DeliverableNotFoundException();
		}
		
		//Only the project owner and the students of a group of the project can access the deliverable
		User u = entityManager.find(User.class, token.getUserID());
		if (u == null) { //checkUserLogin should already have checked this, but just in case...
			throw new InvalidTokenException(); 
		}
		int projectOwnerId = d.getProject().getProfessor().getId();
		if (u instanceof Professor && u.getId() != projectOwnerId) {
			throw new InvalidTokenException();
		} else if (u instanceof Student) {
			List<Integer> projectStudentsIds = new ArrayList<Integer>();
			for (Student s : d.getProject().getStudents()) {
				projectStudentsIds.add(s.getId());
			}
			if (!projectStudentsIds.contains(token.getUserID())) {
				throw new InvalidTokenException();
			}
		}
		
		return d.asDeliverableDetails();
	}

	@Override
	public DeliverableDetails updateDeliverable(LoginToken token,
			int deliverableId, String name, String description,
			Calendar deadline) throws InvalidTokenException,
			DeliverableNotFoundException, DeliverableNameExistsException {
		this.getAccessManager().checkUserLogin(token);
		Deliverable d = entityManager.find(Deliverable.class, deliverableId);
		if (d == null) {
			throw new DeliverableNotFoundException();
		}
		
		if (d.getProject().getProfessor().getId() != token.getUserID()) {
			throw new InvalidTokenException();
		}
		
		if (name != null) {
			d.setName(name);
		}
		if (description != null) {
			d.setDescription(description);
		}
		if (deadline != null) {
			d.setDeadline(deadline); //TODO check deadline validity (e.g. not earlier than today)
		}
		
		entityManager.merge(d);
		
		return d.asDeliverableDetails();
	}

	@Override
	public void deleteDeliverable(LoginToken token, int deliverableId)
			throws InvalidTokenException, DeliverableNotFoundException {
		this.getAccessManager().checkUserLogin(token);
		Deliverable d = entityManager.find(Deliverable.class, deliverableId);
		if (d == null) {
			throw new DeliverableNotFoundException();
		}
		
		if (d.getProject().getProfessor().getId() != token.getUserID()) {
			throw new InvalidTokenException();
		}
		
		entityManager.remove(d);
	}

}
