package mph.session;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mph.entity.Group;
import mph.entity.Project;
import mph.entity.Student;
import mph.exceptions.GroupEmptyException;
import mph.exceptions.GroupLimitReachedException;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProjectNotFoundException;
import mph.exceptions.StudentAlreadyInGroupException;
import mph.exceptions.StudentInMaxOneGroupPerProjectException;
import mph.exceptions.StudentNotFoundException;
import mph.exceptions.StudentNotInGroupException;
import mph.local.GroupManagerLocal;
import mph.util.GroupDetails;
import mph.util.LoginToken;
import mph.util.SecureManager;

import org.jboss.ejb3.annotation.LocalBinding;

/**
 * Session Bean implementation class GroupManager
 */
@Stateless
@LocalBinding(jndiBinding="GroupManagerJNDI")
public class GroupManager extends SecureManager implements GroupManagerLocal {

	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public GroupDetails createGroup(LoginToken token, String name, int projectId)
			throws InvalidTokenException, ProjectNotFoundException {
		this.getAccessManager().checkUserLogin(token);

		Project p = entityManager.find(Project.class, projectId);
		if (p == null) {
			throw new ProjectNotFoundException();
		}
		
		Group g = new Group(name, p);
		entityManager.persist(g);
		
		return g.asGroupDetails();
	}

	@Override
	public GroupDetails findGroup(LoginToken token, int groupId)
			throws InvalidTokenException, GroupNotFoundException {
		this.getAccessManager().checkUserLogin(token);

		Group g = entityManager.find(Group.class, groupId);
		
		if (g == null) {
			throw new GroupNotFoundException();
		}
		
		//Try to assign a final score before returning the group
		g.assignFinalEvaluation();
		
		return g.asGroupDetails();
	}

	@Override
	public GroupDetails addStudentToGroup(LoginToken token, int groupId,
			int studentId) throws InvalidTokenException, GroupNotFoundException, StudentNotFoundException,
			GroupLimitReachedException, StudentInMaxOneGroupPerProjectException, StudentAlreadyInGroupException {
		this.getAccessManager().checkUserLogin(token);
		if (token.getUserID() != studentId) {
			throw new InvalidTokenException();
		}
		Group g = entityManager.find(Group.class, groupId);
		if (g == null) {
			throw new GroupNotFoundException();
		}

		Student s = entityManager.find(Student.class, studentId);
		if (s == null) {
			throw new StudentNotFoundException();
		}
		
		if (g.getMembers().contains(s)) {
			throw new StudentAlreadyInGroupException();
		}
		
		//A student can belong to max one group per project
		//Check that none of groups of the same project already contains the student
		List<Group> groups = g.getProject().getGroups();
		for (Group group : groups) {
			if (group.getMembers().contains(s)) {
				throw new StudentInMaxOneGroupPerProjectException();
			}
		}
		
		g.addStudent(s);
		s.addGroup(g);
		entityManager.merge(g);
		entityManager.merge(s);
		
		return g.asGroupDetails();
	}

	@Override
	public GroupDetails removeStudentFromGroup(LoginToken token, int groupId,
			int studentId) throws InvalidTokenException,
			GroupNotFoundException, StudentNotFoundException,
			StudentNotInGroupException, GroupEmptyException {
		this.getAccessManager().checkUserLogin(token);
		if (token.getUserID() != studentId) {
			throw new InvalidTokenException();
		}
		
		Group g = entityManager.find(Group.class, groupId);
		if (g == null) {
			throw new GroupNotFoundException();
		}

		//Check if the token belongs to a user of the group
		if (!g.asGroupDetails().getMembersIds().contains(new Integer(token.getUserID()))) {
			throw new InvalidTokenException();
		}
		
		Student s = entityManager.find(Student.class, studentId);
		if (s == null) {
			throw new StudentNotFoundException();
		}
		
		g.delStudent(s);
		s.delGroup(g);
		entityManager.merge(g);
		entityManager.merge(s);
		
		return g.asGroupDetails();
	}
}
