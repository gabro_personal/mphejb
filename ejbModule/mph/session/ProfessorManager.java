package mph.session;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import mph.entity.Professor;
import mph.entity.Project;
import mph.exceptions.DeliverableNameExistsException;
import mph.exceptions.DeliverableNotFoundException;
import mph.exceptions.DocumentNotFoundException;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidArgumentsException;
import mph.exceptions.InvalidFileException;
import mph.exceptions.InvalidScoreException;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProfessorNotFoundException;
import mph.exceptions.ProjectNotFoundException;
import mph.exceptions.StudentNotFoundException;
import mph.exceptions.UsernameTakenException;
import mph.remote.ProfessorManagerRemote;
import mph.util.DeliverableDetails;
import mph.util.DocumentDetails;
import mph.util.FieldValidator;
import mph.util.GroupDetails;
import mph.util.LoginToken;
import mph.util.ProfessorDetails;
import mph.util.ProjectDetails;
import mph.util.SecureManager;
import mph.util.StudentDetails;
import mph.util.UserType;

import org.hibernate.exception.ConstraintViolationException;
import org.jboss.ejb3.annotation.RemoteBinding;

/**
 * Session Bean implementation class ProfessorManager
 */
@Stateless
@RemoteBinding(jndiBinding="ProfessorManagerJNDI")
public class ProfessorManager extends SecureManager implements ProfessorManagerRemote {

	@PersistenceContext
	EntityManager entityManager;

	@Override
	public ProfessorDetails createProfessor(LoginToken token, String username,
			String password, String firstName, String lastName, byte[] avatar)
			throws InvalidTokenException, InvalidFileException, UsernameTakenException, InvalidArgumentsException {
		this.getAccessManager().checkUserLogin(token);
		
		//Only an Administrator can create a Professor
		if (this.getAccessManager().getUserType(token) != UserType.ADMINISTRATOR) {
			throw new InvalidTokenException();
		}
		//Validate fields
		if (!FieldValidator.isUsernameValid(username)) {
			throw new InvalidArgumentsException("Invalid username");
		}
		if (!FieldValidator.isPasswordValid(password)) {
			throw new InvalidArgumentsException("Invalid password");
		}
		if (!FieldValidator.isFirstNameValid(firstName)) {
			throw new InvalidArgumentsException("Invalid first name");
		}
		if (!FieldValidator.isLastNameValid(lastName)) {
			throw new InvalidArgumentsException("Invalid last name");
		}

		//TODO check mime type
		Professor p = new Professor(username, password, firstName, lastName, avatar);
		try {
			entityManager.persist(p);
		} catch (PersistenceException e) {
			//Username has a 'unique' constraint
			if(e.getCause() instanceof ConstraintViolationException) {
				throw new UsernameTakenException();
			}
		}
		
		return p.asProfessorDetails();
	}

	@Override
	public ProfessorDetails getProfessor(LoginToken token, int professorId)
			throws InvalidTokenException, ProfessorNotFoundException {
		this.getAccessManager().checkUserLogin(token);
		
		Professor p = entityManager.find(Professor.class, professorId);
		if (p == null) {
			throw new ProfessorNotFoundException();
		}
		
		return p.asProfessorDetails();
	}

	@Override
	public ProfessorDetails updateProfessor(LoginToken token, int professorId,
			String username, String password, String firstName, String lastName, byte[] avatar)
			throws InvalidTokenException, InvalidFileException {
		this.getAccessManager().checkUserLogin(token);
		
		//Only an Administrator can update a Professor
		if (this.getAccessManager().getUserType(token) != UserType.ADMINISTRATOR) {
			throw new InvalidTokenException();
		}
		
		Professor p = entityManager.find(Professor.class, professorId);
		
		if (username != null) {
			p.setUsername(username);
		}
		if (password != null) {
			p.setPassword(password);
		}
		if (firstName != null) {
			p.setFirstName(firstName);
		}
		if (lastName != null) {
			p.setLastName(lastName);
		}
		if (avatar != null) {
			p.setAvatarData(avatar);
		}
		
		entityManager.merge(p);
		
		return p.asProfessorDetails();
	}

	@Override
	public void deleteProfessor(LoginToken token, int professorId)
			throws InvalidTokenException {
		this.getAccessManager().checkUserLogin(token);

		//Only an Administrator can delete a Professor
		if (this.getAccessManager().getUserType(token) != UserType.ADMINISTRATOR) {
			throw new InvalidTokenException();
		}
		
		Professor p = entityManager.find(Professor.class, professorId);
		entityManager.remove(p);
	}

	@Override
	public List<ProjectDetails> getProjectsForProfessor(LoginToken token,
			int professorId) throws InvalidTokenException, ProfessorNotFoundException {
		Professor p = entityManager.find(Professor.class, professorId);
		if (p == null) {
			throw new ProfessorNotFoundException();
		}
		List<ProjectDetails> projectsDetails = new ArrayList<ProjectDetails>();
		for (Project project: p.getProjects()) {
			projectsDetails.add(project.asProjectDetails());
		}
		return projectsDetails;
	}

	@Override
	public ProjectDetails getProject(LoginToken token, int projectId)
			throws InvalidTokenException, ProjectNotFoundException {
		return this.getProjectManager().findProject(token, projectId);
	}

	@Override
	public DocumentDetails evaluateDocument(LoginToken token,
			int documentId, int score) throws InvalidTokenException,
			InvalidScoreException, DocumentNotFoundException {
		return this.getDocumentManager().evaluateDocument(token, documentId, score);
	}

	@Override
	public DocumentDetails getDocument(LoginToken token, int documentId)
			throws InvalidTokenException, DocumentNotFoundException {
		return this.getDocumentManager().findDocument(token, documentId);
	}

	@Override
	public DeliverableDetails createDeliverable(LoginToken token, String name,
			String description, Calendar deadline, int projectId)
			throws InvalidTokenException, ProjectNotFoundException,
			DeliverableNameExistsException {
		return this.getDeliverableManager().createDeliverable(token, name, description, deadline, projectId);
	}

	@Override
	public DeliverableDetails getDeliverable(LoginToken token, int deliverableId)
			throws InvalidTokenException, DeliverableNotFoundException {
		return this.getDeliverableManager().findDeliverable(token, deliverableId);
	}

	@Override
	public DeliverableDetails updateDeliverable(LoginToken token,
			int deliverableId, String name, String description,
			Calendar deadline) throws InvalidTokenException,
			DeliverableNotFoundException, DeliverableNameExistsException {
		return this.getDeliverableManager().updateDeliverable(token, deliverableId, name, description, deadline)
	;
	}

	@Override
	public void deleteDeliverable(LoginToken token, int deliverableId)
			throws InvalidTokenException, DeliverableNotFoundException {
		this.getDeliverableManager().deleteDeliverable(token, deliverableId);
	}

	@Override
	public ProjectDetails createProject(LoginToken token, String name,
			String description, int professorId) throws InvalidTokenException,
			ProfessorNotFoundException {
		return this.getProjectManager().createProject(token, name, description, professorId);
	}

	@Override
	public ProjectDetails updateProject(LoginToken token, int projectId,
			String name, String description) throws InvalidTokenException,
			ProjectNotFoundException {
		return this.getProjectManager().updateProject(token, projectId, name, description);
	}

	@Override
	public GroupDetails getGroup(LoginToken token, int groupId)
			throws InvalidTokenException, GroupNotFoundException {
		return this.getGroupManager().findGroup(token, groupId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProfessorDetails> getAllProfessors(LoginToken token)
			throws InvalidTokenException {
		this.getAccessManager().checkUserLogin(token);
		//Only an Administrator can require this list
		if(this.getAccessManager().getUserType(token) != UserType.ADMINISTRATOR) {
			throw new InvalidTokenException();
		}
		Query q = entityManager.createQuery("SELECT OBJECT(u) FROM User u WHERE UserType='Professor'");
		List<Professor> professors = (List<Professor>)q.getResultList();
		List<ProfessorDetails> professorsDetails = new ArrayList<ProfessorDetails>();
		for (Professor p : professors) {
			professorsDetails.add(p.asProfessorDetails());
		}
		return professorsDetails;
	}

	@Override
	public StudentDetails getStudent(LoginToken token, int studentId)
			throws InvalidTokenException, StudentNotFoundException {
		return this.getStudentManager().getStudent(token, studentId);
	}

	
}
