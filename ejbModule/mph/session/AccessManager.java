package mph.session;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mph.entity.Administrator;
import mph.entity.LoginSession;
import mph.entity.Professor;
import mph.entity.Student;
import mph.entity.User;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.LoginException;
import mph.local.AccessManagerLocal;
import mph.remote.AccessManagerRemote;
import mph.util.LoginToken;
import mph.util.UserType;

import org.jboss.ejb3.annotation.LocalBinding;
import org.jboss.ejb3.annotation.RemoteBinding;

import static mph.util.MagicNumbers.*;

/**
 * Session Bean implementation class AccessManager
 */
@Stateless
@LocalBinding(jndiBinding="AccessManagerJNDILocal")
@RemoteBinding(jndiBinding="AccessManagerJNDI")
public class AccessManager implements AccessManagerLocal, AccessManagerRemote {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public LoginToken login(String username, String password)
			throws LoginException {
		//TODO move this stuff to a findByUsername method in UserManager
		//Retrieve the user by username
		Query q = manager.createQuery("FROM User u WHERE u.username=?1");
		q.setParameter(1, username);
		User user;
		try {
			user = (User)q.getSingleResult();
		} catch (NoResultException e) {
			//User not found
			throw new LoginException();
		}
		
		//Check the password
		LoginToken token;
		if(user.checkPassword(password)) {
			token = new LoginToken(user.getId(), this.getUserSession(user).getId());
			return token;
		} else {
			//Incorrect password
			throw new LoginException();
		}
	}

	@Override
	public void logout(LoginToken token) {
		//TODO manage exceptions
		User u = manager.find(User.class, token.getUserID());
		u.setCurrentSession(null);
		manager.merge(u);
	}

	@Override
	public void checkUserLogin(LoginToken token) throws InvalidTokenException {
		System.out.println("Checking login");
		//Check for validity of the session
		if(isValidToken(token) == false) {
			throw new InvalidTokenException();
		}
		
		//Ok the session is valid.
		
		//Retrieve the valid session and recreate it if null
		LoginSession s = manager.find(LoginSession.class, token.getSessionID());
		if(s == null) {
			User u = manager.find(User.class, token.getUserID());
			s = new LoginSession();
			manager.persist(s);
			u.setCurrentSession(s);
			manager.merge(u);
		} else {
			s.renew();
			manager.merge(s);
		}
	}

	@Override
	public LoginSession getUserSession(User user) {
		if(user.getCurrentSession() == null) {
			LoginSession newSession = new LoginSession();
			manager.persist(newSession);
			user.setCurrentSession(newSession);
		}
		return user.getCurrentSession();
	}
	
	/* 
	 * A token is valid if the user associated has a valid session
	 */
	@Override
	public boolean isValidToken(LoginToken token) {
		if (token == null) {
			return false;
		}
		
		User user = manager.find(User.class, token.getUserID());

		if (user == null) {
			return false;
		}
		LoginSession s = user.getCurrentSession();
		if (s == null) {
			return false;
		}
		
		//Check the session validity
		Calendar validityThreshold = GregorianCalendar.getInstance(); //Get the current time
		validityThreshold.add(Calendar.HOUR, -SESSION_VALIDITY_HOURS); //Subtract the validity hours
		if (s.getTimestamp().before(validityThreshold)) { //If the session is too old is not valid
			return false;
		}
		if (!s.getId().equals(token.getSessionID())) {
			return false;
		}
		return true;
	}

	@Override
	public UserType getUserType(LoginToken token) throws InvalidTokenException {
		this.checkUserLogin(token);
		User u = manager.find(User.class, token.getUserID());
		if (u instanceof Professor) {
			return UserType.PROFESSOR;
		}
		if (u instanceof Student) {
			return UserType.STUDENT;
		}
		if (u instanceof Administrator) {
			return UserType.ADMINISTRATOR;
		}
		throw new InvalidTokenException();
	}
}
