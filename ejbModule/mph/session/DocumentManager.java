package mph.session;

import static mph.util.MagicNumbers.MAX_SCORE;
import static mph.util.MagicNumbers.MIN_SCORE;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mph.entity.Deliverable;
import mph.entity.Document;
import mph.entity.Group;
import mph.entity.Project;
import mph.exceptions.DeliverableNotFoundException;
import mph.exceptions.DocumentAlreadyEvaluatedException;
import mph.exceptions.DocumentAlreadyExistsForDeliverableException;
import mph.exceptions.DocumentNotFoundException;
import mph.exceptions.GroupNotFoundException;
import mph.exceptions.InvalidFileException;
import mph.exceptions.InvalidScoreException;
import mph.exceptions.InvalidTokenException;
import mph.local.DocumentManagerLocal;
import mph.util.DocumentDetails;
import mph.util.LoginToken;
import mph.util.SecureManager;

import org.jboss.ejb3.annotation.LocalBinding;

import static mph.util.MagicNumbers.*;

/**
 * Session Bean implementation class DocumentManager
 */
@Stateless
@LocalBinding(jndiBinding="DocumentManagerJNDI")
public class DocumentManager extends SecureManager implements DocumentManagerLocal {

	@PersistenceContext
	EntityManager entityManager;

	@Override
	public DocumentDetails createDocument(LoginToken token, int deliverableId,
			int groupId, byte[] file) throws InvalidTokenException,
			DeliverableNotFoundException, GroupNotFoundException,
			InvalidFileException, DocumentAlreadyExistsForDeliverableException {
		this.getAccessManager().checkUserLogin(token);
		Group g = entityManager.find(Group.class, groupId);
		if (g == null) {
			throw new GroupNotFoundException();
		}
		
		if (!g.asGroupDetails().getMembersIds().contains(token.getUserID())) {
			throw new InvalidTokenException();
		}
		
		Deliverable d = entityManager.find(Deliverable.class, deliverableId);
		if (d == null) {
			throw new DeliverableNotFoundException();
		}
		
		//TODO Check file validity	
		Document doc = new Document(d, g, file);
		
		//Check if the delivery period is expired
		Calendar deliveryThreshold = GregorianCalendar.getInstance();
		deliveryThreshold.add(Calendar.HOUR, -DELIVERY_GRACE_PERIOD_HOURS);
		if (d.getDeadline().before(deliveryThreshold)) {
			doc.setFinalScore(MIN_SCORE);
		}
		

		entityManager.persist(doc);

		return doc.asDocumentDetails();
	}

	@Override
	public void deleteDocument(LoginToken token, int documentId)
			throws InvalidTokenException, DocumentNotFoundException {
		this.getAccessManager().checkUserLogin(token);
		Document d = entityManager.find(Document.class, documentId);
		if (d == null) {
			throw new DocumentNotFoundException();
		}
		Group g = d.getGroup();
		if (!g.asGroupDetails().getMembersIds().contains(token.getUserID())) {
			throw new InvalidTokenException();
		}
		entityManager.remove(d);
	}

	@Override
	public DocumentDetails evaluateDocument(LoginToken token, int documentId, int score)
			throws InvalidTokenException, DocumentNotFoundException, InvalidScoreException {
		this.getAccessManager().checkUserLogin(token);
		Document d = entityManager.find(Document.class, documentId);
		if (d == null) {
			throw new DocumentNotFoundException();
		}
		int professorId = token.getUserID();
		Project project = d.getGroup().getProject();
		if (professorId != project.getProfessor().getId()) {
			throw new InvalidTokenException();
		}
		
		if (score > MAX_SCORE || score < MIN_SCORE) {
			throw new InvalidScoreException();
		}
		
		d.setScore(score);
				
		entityManager.merge(d);
		
		return d.asDocumentDetails();
	}
	
	@Override
	public DocumentDetails findDocument(LoginToken token, int documentId)
			throws InvalidTokenException, DocumentNotFoundException {
		Document d = entityManager.find(Document.class, documentId);
		if (d == null) {
			throw new DocumentNotFoundException();
		}
		
		Integer projectOwnerId = new Integer(d.getGroup().getProject().getProfessor().getId());
		List<Integer> groupMemebersIds = d.getGroup().asGroupDetails().getMembersIds();
		Integer tokenUserId = new Integer(token.getUserID());
		if (tokenUserId.equals(projectOwnerId) || groupMemebersIds.contains(tokenUserId)) {
			return d.asDocumentDetails();
		} else {
			throw new InvalidTokenException();
		}
		
	}

	@Override
	public DocumentDetails updateDocument(LoginToken token, int documentId, byte[] file)
			throws InvalidTokenException, DocumentNotFoundException, InvalidFileException, DocumentAlreadyEvaluatedException {
		Document d = entityManager.find(Document.class, documentId);
		if (d == null) {
			throw new DocumentNotFoundException();
		}
		if (file == null) {
			throw new InvalidFileException();
		}
		if (d.getScore() > 0) {
			throw new DocumentAlreadyEvaluatedException();
		}
		
		//TODO Check file validity
		
		d.setFile(file);
		d.setDeliveryDate(GregorianCalendar.getInstance());
		entityManager.merge(d);
		
		return d.asDocumentDetails();
	}

}
