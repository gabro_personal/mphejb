package mph.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mph.entity.Professor;
import mph.entity.Project;
import mph.exceptions.InvalidTokenException;
import mph.exceptions.ProfessorNotFoundException;
import mph.exceptions.ProjectNotFoundException;
import mph.local.ProjectManagerLocal;
import mph.util.LoginToken;
import mph.util.ProjectDetails;
import mph.util.SecureManager;

import org.jboss.ejb3.annotation.LocalBinding;

/**
 * Session Bean implementation class ProjectManager
 */
@Stateless
@LocalBinding(jndiBinding="ProjectManagerJNDI")
public class ProjectManager extends SecureManager implements ProjectManagerLocal {

	@PersistenceContext
	EntityManager entityManager;

	@Override
	public ProjectDetails createProject(LoginToken token, String name,
			String description, int professorId) throws InvalidTokenException,
			ProfessorNotFoundException {
		this.getAccessManager().checkUserLogin(token);
		
		Professor prof = entityManager.find(Professor.class, professorId);
		if (prof == null) {
			throw new ProfessorNotFoundException();
		}
		
		Project p = new Project(name, description, prof);
		entityManager.persist(p);
		
		return p.asProjectDetails();
	}

	@Override
	public ProjectDetails findProject(LoginToken token, int projectId)
			throws InvalidTokenException, ProjectNotFoundException {
		this.getAccessManager().checkUserLogin(token);
		
		Project p = entityManager.find(Project.class, projectId);
		if (p == null) {
			throw new ProjectNotFoundException();
		}
		
		return p.asProjectDetails();
	}

	@Override
	public ProjectDetails updateProject(LoginToken token, int projectId,
			String name, String description)
			throws InvalidTokenException, ProjectNotFoundException {
		this.getAccessManager().checkUserLogin(token);
		Project p = entityManager.find(Project.class, projectId);
		if (p == null) {
			throw new ProjectNotFoundException();
		}
		if (name != null) {
			p.setName(name);
		}
		if (description != null) {
			p.setDescription(description);
		}
		entityManager.merge(p);
		return p.asProjectDetails();
	}

}
