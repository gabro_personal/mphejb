/**
 * 
 */
package mph.util;

/**
 * @author Gabriele Petronella
 *
 */
public enum UserType {
	STUDENT,
	PROFESSOR,
	ADMINISTRATOR,
}
