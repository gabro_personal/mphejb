/**
 * 
 */
package mph.util;

import java.io.Serializable;
import java.util.List;

/**
 * @author gabrielepetronella
 *
 */
public class GroupDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private int projectId;
	private int finalScore;
	private List<Integer> membersIds;
	private List<Integer> documentsIds;
	
	/**
	 * @param id
	 * @param membersIds
	 * @param documentsIds
	 */
	public GroupDetails(int id, String name, int projectId, int finalScore, List<Integer> membersIds,
			List<Integer> documentsIds) {
		this.id = id;
		this.projectId = projectId;
		this.finalScore = finalScore;
		this.name = name;
		this.membersIds = membersIds;
		this.documentsIds = documentsIds;
	}

	/**
	 * @return the membersIds
	 */
	public List<Integer> getMembersIds() {
		return membersIds;
	}

	/**
	 * @return the documentsIds
	 */
	public List<Integer> getDocumentsIds() {
		return documentsIds;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	public int getFinalScore() {
		return finalScore;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the projectId
	 */
	public int getProjectId() {
		return projectId;
	}

}
