/**
 * 
 */
package mph.util;

import java.util.List;

/**
 * @author gabrielepetronella
 *
 */
public class ProfessorDetails extends UserDetails {
	private static final long serialVersionUID = 1L;

	private List<Integer> projectsIds;
	
	public ProfessorDetails(int id, String username, String firstName,
			String lastName, List<Integer> projectsIds, byte[] avatar) {
		super(id, username, firstName, lastName, avatar);
		this.projectsIds = projectsIds;
	}

	/**
	 * @return the projectsIds
	 */
	public List<Integer> getProjectsIds() {
		return projectsIds;
	}
	
}
