package mph.util;

import java.io.Serializable;
import static mph.util.MagicNumbers.LOGIN_TOKEN_REGEX;
import static mph.util.MagicNumbers.LOGIN_TOKEN_SEPARATOR;
import mph.exceptions.InvalidTokenStringException;

/**
 * A login token used to keep a user authenticated
 * 
 * @author Gabriele Petronella
 */
public class LoginToken implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int userID;
	private String sessionID;
	
	public LoginToken(int userID, String sessionID) {
		this.userID = userID;
		this.sessionID = sessionID;
	}
	
	public LoginToken(String cookieString) throws InvalidTokenStringException {
		System.out.println(cookieString);
		if (cookieString.matches(LOGIN_TOKEN_REGEX)) {
			String[] ids = cookieString.split(LOGIN_TOKEN_SEPARATOR);
			this.userID = Integer.parseInt(ids[0]);
			this.sessionID = ids[1];
		} else {
			throw new InvalidTokenStringException();
		}
	}
	
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	
	@Override
	public String toString() {
		return userID+"X"+sessionID;
	}
}
