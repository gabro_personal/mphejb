/**
 * 
 */
package mph.util;

import java.io.Serializable;
import java.util.List;

/**
 * @author gabrielepetronella
 *
 */
public class ProjectDetails implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private String description;
	private int professorId;
	private List<Integer> groupsIds;
	private List<Integer> deliverablesIds;
	

	/**
	 * @param id
	 * @param name
	 * @param description
	 * @param professorId
	 * @param groupsIds
	 */
	public ProjectDetails(int id, String name, String description,
			int professorId, List<Integer> groupsIds, List<Integer> deliverablesIds) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.professorId = professorId;
		this.groupsIds = groupsIds;
		this.deliverablesIds = deliverablesIds;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the professorId
	 */
	public int getProfessorId() {
		return professorId;
	}
	/**
	 * @return the groupsIds
	 */
	public List<Integer> getGroupsIds() {
		return groupsIds;
	}

	/**
	 * @return the deliverableIds
	 */
	public List<Integer> getDeliverablesIds() {
		return deliverablesIds;
	}
}
