/**
 * 
 */
package mph.util;

/**
 * @author gabrielepetronella
 *
 */
public class FieldValidator {

	public static boolean isUsernameValid(String username) {
		//Any combination of letters and digits between 3 and 20 characters
		String regex = "^\\w{3,20}$";
		return username.matches(regex);
	}
	
	public static boolean isPasswordValid(String password) {
		String regex = "^\\w{4,}$";
		return password.matches(regex);
		//Any word character (letter, number, underscore) longer than 4 characters
	}
	
	public static boolean isMatriculationNoValid(String matrNo) {
		//Only digits. 6 chars long.
		String regex = "^\\d{6}";
		return matrNo.matches(regex);
	}
	
	public static boolean isFirstNameValid(String firstName) {
		//Any character between 2 and 25 characters
		String regex = "^[a-zA-Z]{2,25}$";
		return firstName.matches(regex);
	}
	
	public static boolean isLastNameValid(String lastName) {
		//Any character between 2 and 25 characters
		String regex = "^[a-zA-Z]{2,25}$";
		return lastName.matches(regex);
	}
}
