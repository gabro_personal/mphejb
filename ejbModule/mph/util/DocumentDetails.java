/**
 * 
 */
package mph.util;

import java.io.Serializable;
import java.util.Calendar;

/**
 * @author gabrielepetronella
 *
 */
public class DocumentDetails implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int id;
	private int deliverableId;
	private int groupId;
	private byte[] fileData;
	private int score;
	private int malus;
	private int finalScore;
	private Calendar deliveryDate;
	
	/**
	 * @param id
	 * @param deliverableId
	 * @param groupId
	 * @param file
	 */
	public DocumentDetails(int id, int deliverableId, int groupId, byte[] file, int score, int malus, int finalScore, Calendar deliveryDate) {
		this.id = id;
		this.deliverableId = deliverableId;
		this.groupId = groupId;
		this.fileData = file;
		this.score = score;
		this.malus = malus;
		this.finalScore = finalScore;
		this.deliveryDate = deliveryDate;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the deliverableId
	 */
	public int getDeliverableId() {
		return deliverableId;
	}

	/**
	 * @return the groupId
	 */
	public int getGroupId() {
		return groupId;
	}

	/**
	 * @return the file
	 */
	public byte[] getFileData() {
		return fileData;
	}

	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}
	
	/**
	 * @return the malus
	 */
	public int getMalus() {
		return malus;
	}
	
	/**
	 * @return the finalScore
	 */
	public int getFinalScore() {
		return finalScore;
	}

	/**
	 * @return the deliveryDate
	 */
	public Calendar getDeliveryDate() {
		return deliveryDate;
	}
}
