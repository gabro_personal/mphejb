/**
 * 
 */
package mph.util;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import mph.local.AccessManagerLocal;
import mph.local.DeliverableManagerLocal;
import mph.local.DocumentManagerLocal;
import mph.local.GroupManagerLocal;
import mph.local.ProjectManagerLocal;
import mph.remote.ProfessorManagerRemote;
import mph.remote.StudentManagerRemote;

/**
 * A commodity abstract class extended by all the session beans.
 * Features the methods to perform every needed JNDI lookup
 * 
 * @author Gabriele Petronella
 */
public abstract class SecureManager {

	public AccessManagerLocal getAccessManager() {
		AccessManagerLocal accessManager = null;
		try {
			accessManager = (AccessManagerLocal) new InitialContext().lookup("AccessManagerJNDILocal");
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return accessManager;
	}
		
	public StudentManagerRemote getStudentManager() {
		StudentManagerRemote studentManager = null;
		try {
			studentManager = (StudentManagerRemote) new InitialContext().lookup("StudentManagerJNDI");
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return studentManager;
	}
	
	public ProfessorManagerRemote getProfessorManager() {
		ProfessorManagerRemote professorManager = null;
		try {
			professorManager = (ProfessorManagerRemote) new InitialContext().lookup("ProfessorManagerJNDI");
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return professorManager;
	}
	
	public GroupManagerLocal getGroupManager() {
		GroupManagerLocal groupManager = null;
		try {
			groupManager = (GroupManagerLocal) new InitialContext().lookup("GroupManagerJNDI");
		} catch (NamingException e) {
			e.printStackTrace();
			System.err.println("Error connecting to GroupManager");
		}
		return groupManager;
	}
	
	public ProjectManagerLocal getProjectManager() {
		ProjectManagerLocal projectManager = null;
		try {
			projectManager = (ProjectManagerLocal) new InitialContext().lookup("ProjectManagerJNDI");
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return projectManager;
	}
	
	public DocumentManagerLocal getDocumentManager() {
		DocumentManagerLocal documentManager = null;
		try {
			documentManager = (DocumentManagerLocal) new InitialContext().lookup("DocumentManagerJNDI");
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return documentManager;
	}
	
	public DeliverableManagerLocal getDeliverableManager() {
		DeliverableManagerLocal deliverableManager = null;
		try {
			deliverableManager = (DeliverableManagerLocal) new InitialContext().lookup("DeliverableManagerJNDI");
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return deliverableManager;
	}
}