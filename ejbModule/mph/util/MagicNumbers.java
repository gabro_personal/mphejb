/**
 * 
 */
package mph.util;

/**
 * Magic numbers (and strings)
 * 
 * @author Gabriele Petronella
 */
public class MagicNumbers {

	/**
	 * The minimum assignable score for a document
	 */
	public static final int MIN_SCORE = 1;
	
	/**
	 * The maximum assignable score for a document
	 */
	public static final int MAX_SCORE = 10;
	
	/**
	 * The maximum number of students in each group
	 */
	public static final int MAX_STUDENTS_PER_GROUP = 3;
	
	/**
	 * The hours of validity of a Login Session
	 */
	public static final int SESSION_VALIDITY_HOURS = 1;
	
	/**
	 * The interval (in hours) for increasing malus by 1 point
	 */
	public static final int MALUS_RATE_HOURS = 24;
	
	/**
	 * The maximum tolerated delay (in hours) for delivering a document
	 */
	public static final int DELIVERY_GRACE_PERIOD_HOURS = 10*24; //Maximum delivery delay tolerated
	
	public static final String LOGIN_TOKEN_SEPARATOR = "X";
	public static final String LOGIN_TOKEN_REGEX = "^[0-9]+"+LOGIN_TOKEN_SEPARATOR+"[a-f0-9]{32}$";
}
