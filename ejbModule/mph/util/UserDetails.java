/**
 * 
 */
package mph.util;

import java.io.Serializable;

/**
 * A client representation of the User
 * @author Gabriele
 *
 */
public class UserDetails implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String username;
	private String firstName;
	private String lastName;
	private byte[] avatar;

	/**
	 * @param username
	 * @param firstName
	 * @param lastName
	 */
	public UserDetails(int id, String username, String firstName, String lastName, byte[] avatar) {
		this.id = id;
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.avatar = avatar;
	}
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the avatar
	 */
	public byte[] getAvatar() {
		return avatar;
	}
}
