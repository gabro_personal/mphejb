/**
 * 
 */
package mph.util;

import java.util.List;

/**
 * @author gabrielepetronella
 *
 */
public class StudentDetails extends UserDetails {
	private static final long serialVersionUID = 1L;

	private String matrNo;
	private List<Integer> groupsIds;

	/**
	 * @param id
	 * @param username
	 * @param firstName
	 * @param lastName
	 */
	public StudentDetails(int id, String username, String firstName,
			String lastName, String matrNo, List<Integer> groupsIds, byte[] avatar) {
		super(id, username, firstName, lastName, avatar);
		this.matrNo = matrNo;
		this.groupsIds = groupsIds;
	}
	
	/**
	 * @return the matrNo
	 */
	public String getMatrNo() {
		return matrNo;
	}

	/**
	 * @return the groupsIds
	 */
	public List<Integer> getGroupsIds() {
		return groupsIds;
	}
}
