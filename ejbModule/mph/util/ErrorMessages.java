/**
 * 
 */
package mph.util;

import static mph.util.MagicNumbers.*;

/**
 * @author gabrielepetronella
 *
 */
public class ErrorMessages {

	public static final String DELIVERABLE_NAME_EXISTS = "Deliverable name already exists";
	public static final String DELIVERABLE_NOT_FOUND = "Deliverable not found";
	public static final String DOCUMENT_ALREADY_EXISTS_FOR_DELIVERABLE = "A document already exists for this deliverable";
	public static final String DOCUMENT_ALREADY_EVALUATED = "Document has already been evaluated. You cannot edit it";
	public static final String DOCUMENT_NOT_IN_GROUP = "Document is not in this group";
	public static final String DOCUMENT_NOT_FOUND = "Document not found";
	public static final String GROUP_EMPTY_EXCEPTION = "Group is empty";
	public static final String GROUP_LIMIT_REACHED = "Group is full";
	public static final String GROUP_NOT_FOUND = "Group not found";
	public static final String INVALID_ARGUMENTS = "Invalid argumetns";
	public static final String INVALID_SCORE_EXCEPTION = "Score must be between "+MIN_SCORE+" and "+MAX_SCORE;
	public static final String INVALID_TOKEN = "Access denied";
	public static final String LOGIN_EXCEPTION = "Login failed";
	public static final String PROFESSOR_NOT_FOUND = "Professor not found";
	public static final String PROJECT_NOT_FOUND = "Project not found";
	public static final String STUDENT_ALREADY_IN_GROUP = "Student already belongs to this group";
	public static final String STUDENT_IN_MAX_ONE_GROUP_PER_PROJECT = "Student is already working to this project";
	public static final String STUDENT_NOT_FOUND = "Student not found";
	public static final String STUDENT_NOT_IN_GROUP = "Student not in this group";
	public static final String USERNAME_TAKEN = "Username is already taken";
}
