/**
 * 
 */
package mph.util;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

/**
 * @author gabrielepetronella
 *
 */
public class DeliverableDetails implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int id;
	private int projectId;
	private String name;
	private String description;
	private Calendar deadline;
	private List<Integer> documentsIds;
	
	/**
	 * @param id
	 * @param projectId
	 * @param name
	 * @param description
	 * @param documentsIds
	 */
	public DeliverableDetails(int id, int projectId, String name,
			String description, Calendar deadline, List<Integer> documentsIds) {
		this.id = id;
		this.projectId = projectId;
		this.name = name;
		this.description = description;
		this.deadline = deadline;
		this.documentsIds = documentsIds;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @return the projectId
	 */
	public int getProjectId() {
		return projectId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @return the deadline
	 */
	public Calendar getDeadline() {
		return deadline;
	}
	/**
	 * @return the documentsIds
	 */
	public List<Integer> getDocumentsIds() {
		return documentsIds;
	}
}
